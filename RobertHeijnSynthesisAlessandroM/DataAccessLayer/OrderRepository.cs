﻿using DomainLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainLayer;
using DomainLayer.Enums;
using System.Reflection.PortableExecutable;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Common;

namespace DataAccessLayer
{
    [Serializable]
    public class OrderRepository : IOrderRepository
    {
        MySqlConnection sqlConnection;

        public OrderRepository()
        {
            string connectionInfo = "Server=studmysql01.fhict.local;Uid=dbi501938;Database=dbi501938;Pwd=synthesiss2;";
            this.sqlConnection = new MySqlConnection(connectionInfo);
        }

        public List<Order> GetCustomerOrders(int id)
        {
            List<Order> result = new List<Order>();
            if (id != 0)
            {
                try
                {
                    sqlConnection.Open();
                    MySqlCommand command;
                    command = new MySqlCommand("SELECT orders.Id as OrderId, orders.CustomerId, orders.OrderStatus, orders.PaymentMethod, orders.OrderDate, orders.DeliveryDate, orderproducts.ProductId, orderproducts.ProductPrice, orderproducts.Quantity, orderproducts.Discount, products.Name, products.SubCategory, products.Category, products.Unit FROM orders INNER JOIN orderproducts ON orders.Id=orderproducts.OrderId INNER JOIN products ON orderproducts.ProductId=products.Id WHERE CustomerId=@CustomerId", sqlConnection);
                    command.Parameters.AddWithValue("@CustomerId", id);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Order order = result.FirstOrDefault(order => order.Id == (int)reader["OrderId"]);

                        if (order == null)
                        {
                            order = new Order((int)reader["OrderId"], Convert.ToDateTime(reader["OrderDate"]), Convert.ToDateTime(reader["DeliveryDate"]), (OrderStatus)Convert.ToInt32(reader["OrderStatus"]), (PaymentMethod)Convert.ToInt32(reader["PaymentMethod"]));

                            result.Add(order);
                        }
                        OrderItem item = new OrderItem(new Product((int)reader["ProductId"], reader["Name"].ToString(), reader["SubCategory"].ToString(), reader["Category"].ToString(), Convert.ToDouble(reader["ProductPrice"]), (Unit)reader["Unit"]), (int)reader["Quantity"], Convert.ToDouble(reader["Discount"]));

                        order.Add(item);
                    }
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            return result;
        }

        public List<Order> GetAll()
        {
            List<Order> result = new List<Order>();
            try
            {
                sqlConnection.Open();
                MySqlCommand command;
                command = new MySqlCommand("SELECT orders.Id as OrderId, orders.CustomerId, orders.OrderStatus, orders.PaymentMethod, orders.OrderDate, orders.DeliveryDate, orderproducts.ProductId, orderproducts.ProductPrice, orderproducts.Quantity, orderproducts.Discount, products.Name, products.SubCategory, products.Category, products.Unit FROM orders INNER JOIN orderproducts ON orders.Id=orderproducts.OrderId INNER JOIN products ON orderproducts.ProductId=products.Id;", sqlConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Order order = result.FirstOrDefault(order => order.Id == (int)reader["OrderId"]);

                    if (order == null)
                    {
                        order = new Order((int)reader["OrderId"], Convert.ToDateTime(reader["OrderDate"]), Convert.ToDateTime(reader["DeliveryDate"]), (OrderStatus)Convert.ToInt32(reader["OrderStatus"]), (PaymentMethod)Convert.ToInt32(reader["PaymentMethod"]));

                        result.Add(order);
                    }
                    OrderItem item = new OrderItem(new Product((int)reader["ProductId"], reader["Name"].ToString(), reader["SubCategory"].ToString(), reader["Category"].ToString(), Convert.ToDouble(reader["ProductPrice"]), (Unit)reader["Unit"]), (int)reader["Quantity"], Convert.ToDouble(reader["Discount"]));
                
                    order.Add(item);
                }
            }
            finally
            {
                sqlConnection.Close();
            }
            return result;
        }

        public void UpdateStatus(Order order)
        {
            try
            {
                sqlConnection.Open();
                var command = new MySqlCommand("UPDATE orders WHERE id=@id SET OrderStatus=@OrderStatus", sqlConnection);

                command.Parameters.AddWithValue("@id", order.Id);
                command.Parameters.AddWithValue("@OrderStatus", (int)order.OrderStatus);
                command.ExecuteNonQuery();
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public void Add(Order order)
        {
            try
            {
                sqlConnection.Open();
                var sql = @"
                    INSERT INTO `orders` (CustomerId, OrderStatus, PaymentMethod, OrderDate, DeliveryDate) VALUES(@CustomerId, @OrderStatus, @PaymentMethod, @OrderDate, @DeliveryDate);

                    SELECT LAST_INSERT_ID();
                ";

                // ToDo: Should store the delivery address in this table as well, otherwise the address may change in time.
                
                var cmd = new MySqlCommand(sql, this.sqlConnection);

                cmd.Parameters.AddWithValue("@CustomerId", order.Customer.Id);
                cmd.Parameters.AddWithValue("@OrderStatus", order.OrderStatus);
                cmd.Parameters.AddWithValue("@PaymentMethod", Convert.ToInt32(order.PaymentMethod));
                cmd.Parameters.AddWithValue("@OrderDate", order.OrderDate);
                cmd.Parameters.AddWithValue("@DeliveryDate", order.DeliveryDate);

                int OrderId = Convert.ToInt32(cmd.ExecuteScalar());

                sql = @"
                    INSERT INTO `orderproducts` (OrderId, ProductId, ProductPrice, Quantity, Discount) VALUES(@OrderId, @ProductId, @ProductPrice, @Quantity, @Discount);
                ";

                foreach (OrderItem orderItem in order.OrderItems)
                {
                    cmd = new MySqlCommand(sql, this.sqlConnection);
                    cmd.Parameters.AddWithValue("@OrderId", OrderId);
                    cmd.Parameters.AddWithValue("@ProductId", orderItem.Product.Id);
                    cmd.Parameters.AddWithValue("@ProductPrice", orderItem.Product.Price);
                    cmd.Parameters.AddWithValue("@Quantity", orderItem.Quantity);
                    cmd.Parameters.AddWithValue("@Discount", orderItem.Discount);
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }
}
