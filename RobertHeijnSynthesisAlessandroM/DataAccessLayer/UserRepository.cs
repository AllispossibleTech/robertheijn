﻿using DomainLayer;
using DomainLayer.Enums;
using DomainLayer.Interfaces;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Common;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;

namespace DataAccessLayer
{
    [Serializable]
    public class UserRepository : IUserRepository
    {
        MySqlConnection sqlConnection;
        IOrderRepository orderReposutitory;
        public UserRepository()
        {
            string connectionInfo = "Server=studmysql01.fhict.local;Uid=dbi501938;Database=dbi501938;Pwd=synthesiss2;";
            this.orderReposutitory = new OrderRepository();
            this.sqlConnection = new MySqlConnection(connectionInfo);

        }
        public void Add(User user)
        {
            try
            {
                sqlConnection.Open();
                var sql = @"
                    INSERT INTO users (FirstName, Surname, Birthdate, Phone, UserType) VALUES(@FirstName, @Surname, @Birthdate, @Phone, @UserType);

                    DECLARE @id AS int = SELECT SCOPE_IDENTITY();

                    INSERT INTO addresses (UserId, Street, HouseNumber, City, PostalCode, Country) VALUES (@id, @Street, @HouseNumber, @City, @PostalCode, @Country);
                ";
                if (user is Employee)
                {
                    sql = @"
                        INSERT INTO users (FirstName, Surname, Birthdate, Phone, UserType) VALUES(@FirstName, @Surname, @Birthdate, @Phone, @UserType);

                        INSERT INTO credentials (UserId, Email, Password) VALUES(LAST_INSERT_ID(), @Email, @Password);

                        INSERT INTO addresses (UserId, Street, HouseNumber, City, PostalCode, Country) VALUES (LAST_INSERT_ID(), @Street, @HouseNumber, @City, @PostalCode, @Country);

                        INSERT INTO employees (UserId, ContractStart, Position) VALUES(LAST_INSERT_ID(), @ContractStart, @Position);
                    ";
                }
                else if (user is Customer)
                {
                    sql = @"
                        START TRANSACTION;

                        INSERT INTO users (FirstName, Surname, Birthdate, Phone, UserType) VALUES(@FirstName, @Surname, @Birthdate, @Phone, @UserType);


                        INSERT INTO credentials (UserId, Email, Password) VALUES(LAST_INSERT_ID(), @Email, @Password);
                        INSERT INTO addresses (UserId, Street, HouseNumber, City, PostalCode, Country) VALUES (LAST_INSERT_ID(), @Street, @HouseNumber, @City, @PostalCode, @Country);
                        INSERT INTO customers (UserId, BonusCardPoints) VALUES(LAST_INSERT_ID(), @BonusCardPoints);
                        COMMIT;
                    ";
                }

                var cmd = new MySqlCommand(sql, this.sqlConnection);

                cmd.Parameters.AddWithValue("@Email", user.Email);
                cmd.Parameters.AddWithValue("@Password", user.Password);
                cmd.Parameters.AddWithValue("@FirstName", user.FirstName);
                cmd.Parameters.AddWithValue("@Surname", user.Surname);
                cmd.Parameters.AddWithValue("@Birthdate", user.BirthDate);
                cmd.Parameters.AddWithValue("@Street", user.Address.Street);
                cmd.Parameters.AddWithValue("@HouseNumber", user.Address.HouseNumber);
                cmd.Parameters.AddWithValue("@City", user.Address.City);
                cmd.Parameters.AddWithValue("@PostalCode", user.Address.PostalCode);
                cmd.Parameters.AddWithValue("@Country", user.Address.Country);
                cmd.Parameters.AddWithValue("@Phone", user.PhoneNumber);

                if (user is Employee)
                {
                    cmd.Parameters.AddWithValue("@ContractStart", ((Employee)user).ContractStart);
                    cmd.Parameters.AddWithValue("@Position", ((Employee)user).Position);
                    cmd.Parameters.AddWithValue("@UserType", 0);
                }
                else if (user is Customer)
                {
                    cmd.Parameters.AddWithValue("@BonusCardPoints", ((Customer)user).BonusCard.Points);
                    cmd.Parameters.AddWithValue("@UserType", 1);
                }

                cmd.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public void Delete(User user)
        {
            throw new NotImplementedException();
        }

        public bool Exists(User user)
        {
            try
            {
                sqlConnection.Open();
                var command = new MySqlCommand("SELECT id FROM `users` WHERE id=@id;", sqlConnection);
                command.Parameters.AddWithValue("@id", user.Id);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    return true;
                }
                sqlConnection.Close();
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<User> GetUsers()
        {
            List<User> result = new List<User>();
            try
            {
                sqlConnection.Open();
                
                var command = new MySqlCommand("SELECT users.id, users.FirstName, users.Surname, users.Birthdate, users.Phone, users.UserType, credentials.Email, credentials.Password, addresses.Street, addresses.HouseNumber, addresses.City, addresses.PostalCode, addresses.Country, customers.BonusCardPoints, NULL as ContractStart, NULL as Position FROM users INNER JOIN customers ON users.Id=customers.UserId INNER JOIN addresses ON users.Id=addresses.UserId INNER JOIN credentials ON users.Id=credentials.UserId WHERE users.UserType=1 UNION SELECT users.id, users.FirstName, users.Surname, users.Birthdate, users.Phone, users.UserType, credentials.Email, credentials.Password, addresses.Street, addresses.HouseNumber, addresses.City, addresses.PostalCode, addresses.Country, NULL as BonusCardPoints, employees.ContractStart, employees.Position FROM users INNER JOIN employees ON users.Id=employees.UserId INNER JOIN addresses ON users.Id=addresses.UserId INNER JOIN credentials ON users.Id=credentials.UserId WHERE users.UserType=0;", sqlConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if ((UserType)Convert.ToInt32(reader["UserType"]) == UserType.CUSTOMER)
                    {
                        result.Add(new Customer(

                        Convert.ToInt32(reader["id"]),
                        reader["Email"].ToString().Trim(),
                        reader["Password"].ToString().Trim(),
                        reader["FirstName"].ToString().Trim(),
                        reader["Surname"].ToString().Trim(),
                        Convert.ToDateTime(reader["Birthdate"].ToString().Trim()),

                        new Address(reader["Street"].ToString(), (int)reader["HouseNumber"], reader["City"].ToString(), reader["PostalCode"].ToString(), reader["Country"].ToString()),

                        reader["Phone"].ToString(),

                        new BonusCard(Convert.ToInt32(reader["id"]), Convert.ToInt32(reader["BonusCardPoints"])),

                        this.orderReposutitory.GetCustomerOrders(Convert.ToInt32(reader["id"]))
                        ));
                    }
                    else if ((UserType)Convert.ToInt32(reader["UserType"]) == UserType.EMPLOYEE)
                    {
                        result.Add(new Employee(
                            Convert.ToInt32(reader["id"]),
                            reader["Email"].ToString().Trim(),
                            reader["Password"].ToString().Trim(),
                            reader["FirstName"].ToString().Trim(),
                            reader["Surname"].ToString().Trim(),
                            Convert.ToDateTime(reader["Birthdate"].ToString().Trim()),

                            new Address(reader["Street"].ToString(), (int)reader["HouseNumber"], reader["City"].ToString(), reader["PostalCode"].ToString(), reader["Country"].ToString()),

                            reader["Phone"].ToString(),
                            Convert.ToDateTime(reader["ContractStart"]),
                            reader["Position"].ToString()
                        ));
                    }
                }
                sqlConnection.Close();
                return result;
            }
            catch (Exception e) 
            {
                throw e;
            }
        }
    } 
}
