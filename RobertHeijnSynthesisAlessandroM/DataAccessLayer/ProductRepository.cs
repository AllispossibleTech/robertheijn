﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainLayer;
using DomainLayer.Advertisements;
using DomainLayer.Enums;
using DomainLayer.Interfaces;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Common;
using Org.BouncyCastle.Asn1.X509;

namespace DataAccessLayer
{
    [Serializable]
    public class ProductRepository : IProductRepository
    {
        MySqlConnection sqlConnection;

        public ProductRepository()
        {
            string connectionInfo = "Server=studmysql01.fhict.local;Uid=dbi501938;Database=dbi501938;Pwd=synthesiss2;";
            this.sqlConnection = new MySqlConnection(connectionInfo);
        }

        public void Add(Product product)
        {
            // ToDo: fix this lot of tedious code, too many repetitions: could just pass null without checking.
            try
            {
                sqlConnection.Open();

                var sql = @"
                    INSERT INTO `products` (Name, SubCategory, Category, Price, Unit) VALUES(@Name, @SubCategory, @Category, @Price, @Unit);
                ";

                var cmd = new MySqlCommand(sql, this.sqlConnection);

                if (product.Weight != null)
                {
                    sql = @"
                        INSERT INTO `products` (Name, SubCategory, Category, Price, Unit, Weight) VALUES(@Name, @SubCategory, @Category, @Price, @Unit, @Weight);
                    ";

                    cmd = new MySqlCommand(sql, this.sqlConnection);

                    cmd.Parameters.AddWithValue("@Name", product.Name);
                    cmd.Parameters.AddWithValue("@SubCategory", product.SubCategory);
                    cmd.Parameters.AddWithValue("@Category", product.Category);
                    cmd.Parameters.AddWithValue("@Price", product.Price);
                    cmd.Parameters.AddWithValue("@Unit", product.Unit);
                    cmd.Parameters.AddWithValue("@Weight", product.Weight);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Name", product.Name);
                    cmd.Parameters.AddWithValue("@SubCategory", product.SubCategory);
                    cmd.Parameters.AddWithValue("@Category", product.Category);
                    cmd.Parameters.AddWithValue("@Price", product.Price);
                    cmd.Parameters.AddWithValue("@Unit", product.Unit);
                }

                cmd.ExecuteNonQuery();
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public Product Get(int productId)
        {
            try
            {
                sqlConnection.Open();
                var command = new MySqlCommand("SELECT * FROM `products` WHERE Id=@Id;", sqlConnection);
                command.Parameters.AddWithValue("@Id", productId);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (!DBNull.Value.Equals(reader["Weight"]))
                    {
                        return new Product(

                        Convert.ToInt32(reader["Id"]),
                        reader["Name"].ToString(),
                        reader["SubCategory"].ToString(),
                        reader["Category"].ToString(),
                        Convert.ToDouble(reader["Price"]),
                        (Unit)reader["Unit"],
                        Convert.ToDouble(reader["Weight"])
                        );
                    }
                    else
                    {
                        return new Product(

                        Convert.ToInt32(reader["Id"]),
                        reader["Name"].ToString(),
                        reader["SubCategory"].ToString(),
                        reader["Category"].ToString(),
                        Convert.ToDouble(reader["Price"]),
                        (Unit)reader["Unit"]
                        );
                    }

                }
                
                return null;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public List<Product> GetAll()
        {
            List<Product> result = new List<Product>();
            try
            {
                sqlConnection.Open();
                var command = new MySqlCommand("SELECT * FROM `products`;", sqlConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (!DBNull.Value.Equals(reader["Weight"]))
                    {
                        result.Add(new Product(

                        Convert.ToInt32(reader["Id"]),
                        reader["Name"].ToString(),
                        reader["SubCategory"].ToString(),
                        reader["Category"].ToString(),
                        Convert.ToDouble(reader["Price"]),
                        (Unit)reader["Unit"],
                        Convert.ToDouble(reader["Weight"])
                        ));
                    }
                    else
                    {
                        result.Add(new Product(

                        Convert.ToInt32(reader["Id"]),
                        reader["Name"].ToString(),
                        reader["SubCategory"].ToString(),
                        reader["Category"].ToString(),
                        Convert.ToDouble(reader["Price"]),
                        (Unit)reader["Unit"]
                        ));
                    }
                    
                }
                
                return result;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public int GetStockLevel(int productId)
        {
            throw new NotImplementedException();
        }

        public void Remove(int productId)
        {
            try
            {
                sqlConnection.Open();
                var command = new MySqlCommand("DELETE FROM `products` WHERE Id=@Id;", sqlConnection);
                command.Parameters.AddWithValue("@Id", productId);
                command.ExecuteNonQuery();
                
            }
            finally 
            { 
                sqlConnection.Close(); 
            }
        }
    }
}
