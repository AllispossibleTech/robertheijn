﻿using DomainLayer;
using DomainLayer.Advertisements;
using DomainLayer.Enums;
using DomainLayer.Interfaces;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    [Serializable]
    public class AdvertisementRepository : IAdvertisementRepository
    {
        MySqlConnection sqlConnection;
        IProductRepository productRepository;

        [JsonConstructor]
        public AdvertisementRepository()
        {
            this.productRepository = new ProductRepository();
            string connectionInfo = "Server=studmysql01.fhict.local;Uid=dbi501938;Database=dbi501938;Pwd=synthesiss2;";
            this.sqlConnection = new MySqlConnection(connectionInfo);
        }

        public void Add(IProductDiscountAdvertisement advertisement)
        {
            //sqlConnection.Close();
            try
            {
                sqlConnection.Open();

                var sql = @"
                        INSERT INTO `advertisements` (ProductId, StartDate, EndDate, DiscountType) VALUES(@ProductId, @StartDate, @EndDate, @DiscountType);
                    ";
                var cmd = new MySqlCommand(sql, this.sqlConnection);

                if (advertisement is AD_TwoForOne)
                {
                    cmd = new MySqlCommand(sql, this.sqlConnection);

                    cmd.Parameters.AddWithValue("@ProductId", ((AD_TwoForOne)advertisement).Product.Id);
                    cmd.Parameters.AddWithValue("@StartDate", ((AD_TwoForOne)advertisement).StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", ((AD_TwoForOne)advertisement).EndDate);
                    cmd.Parameters.AddWithValue("@DiscountType", (int)DiscountType.TWO_FOR_ONE);
                }
                else if (advertisement is AD_SecondHalfPrice)
                {
                    cmd = new MySqlCommand(sql, this.sqlConnection);

                    cmd.Parameters.AddWithValue("@ProductId", ((AD_SecondHalfPrice)advertisement).Product.Id);
                    cmd.Parameters.AddWithValue("@StartDate", ((AD_SecondHalfPrice)advertisement).StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", ((AD_SecondHalfPrice)advertisement).EndDate);
                    cmd.Parameters.AddWithValue("@DiscountType", (int)DiscountType.SECOND_HALF_PRICE);
                }
                else if (advertisement is AD_TwoForPrice)
                {
                    sql = @"
                    INSERT INTO `advertisements` (ProductId, StartDate, EndDate, DiscountType, Price) VALUES(@ProductId, @StartDate, @EndDate, @DiscountType, @Price);
                    ";

                    cmd = new MySqlCommand(sql, this.sqlConnection);

                    cmd.Parameters.AddWithValue("@ProductId", ((AD_TwoForPrice)advertisement).Product.Id);
                    cmd.Parameters.AddWithValue("@StartDate", ((AD_TwoForPrice)advertisement).StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", ((AD_TwoForPrice)advertisement).EndDate);
                    cmd.Parameters.AddWithValue("@DiscountType", (int)DiscountType.TWO_FOR_PRICE);
                    cmd.Parameters.AddWithValue("@Price", ((AD_TwoForPrice)advertisement).Price);
                }
                else if (advertisement is AD_PriceForWeight)
                {
                    sql = @"
                    INSERT INTO `advertisements` (ProductId, StartDate, EndDate, DiscountType, Unit, Price, Weight) VALUES(@ProductId, @StartDate, @EndDate, @DiscountType, @Unit, @Price, @Weight);
                    ";

                    cmd = new MySqlCommand(sql, this.sqlConnection);

                    cmd.Parameters.AddWithValue("@ProductId", ((AD_PriceForWeight)advertisement).Product.Id);
                    cmd.Parameters.AddWithValue("@StartDate", ((AD_PriceForWeight)advertisement).StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", ((AD_PriceForWeight)advertisement).EndDate);
                    cmd.Parameters.AddWithValue("@DiscountType", (int)DiscountType.TWO_FOR_PRICE);
                    cmd.Parameters.AddWithValue("@Unit", ((AD_PriceForWeight)advertisement).Unit);
                    cmd.Parameters.AddWithValue("@Price", ((AD_PriceForWeight)advertisement).Price);
                    cmd.Parameters.AddWithValue("@Weight", ((AD_PriceForWeight)advertisement).Weight);
                }

                cmd.ExecuteNonQuery();
            }
            finally
            {
                sqlConnection.Close();
            }
        }


        public IProductDiscountAdvertisement Get(int id)
        {
            try
            {
                sqlConnection.Open();
                var command = new MySqlCommand("SELECT * FROM `advertisements` WHERE ProductId=@ProductId;", sqlConnection);
                command.Parameters.AddWithValue("@ProductId", id);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    DiscountType discountType = (DiscountType)reader["DiscountType"];

                    if (discountType == DiscountType.TWO_FOR_PRICE)
                    {
                        return new AD_TwoForPrice(

                            this.productRepository.Get(Convert.ToInt32(reader["ProductId"])),
                            Convert.ToDateTime(reader["StartDate"]),
                            Convert.ToDateTime(reader["EndDate"]),
                            Convert.ToDouble(reader["Price"])
                        );
                    }
                    else if (discountType == DiscountType.SECOND_HALF_PRICE)
                    {
                        return new AD_SecondHalfPrice(

                            this.productRepository.Get(Convert.ToInt32(reader["ProductId"])),
                            Convert.ToDateTime(reader["StartDate"]),
                            Convert.ToDateTime(reader["EndDate"])
                        );
                    }
                    else if (discountType == DiscountType.TWO_FOR_ONE)
                    {
                        return new AD_TwoForOne(

                            this.productRepository.Get(Convert.ToInt32(reader["ProductId"])),
                            Convert.ToDateTime(reader["StartDate"]),
                            Convert.ToDateTime(reader["EndDate"])
                        );
                    }
                    else if (discountType == DiscountType.PRICE_FOR_WEIGHT)
                    {
                        return new AD_PriceForWeight(

                            this.productRepository.Get(Convert.ToInt32(reader["ProductId"])),
                            Convert.ToDateTime(reader["StartDate"]),
                            Convert.ToDateTime(reader["EndDate"]),
                            Convert.ToDouble(reader["Price"]),
                            Convert.ToDouble(reader["Weight"]),
                            (Unit)reader["Unit"]
                        );
                    }
                }
                return null;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        /// <summary>
        /// Returns all types of advertisements in a list
        /// </summary>
        /// <returns></returns>
        public List<IProductDiscountAdvertisement> GetAll()
        {
            List<IProductDiscountAdvertisement> result = new List<IProductDiscountAdvertisement>();
            try
            {
                sqlConnection.Open();
                var command = new MySqlCommand("SELECT * FROM `advertisements`;", sqlConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    DiscountType discountType = (DiscountType)reader["DiscountType"];

                    if (discountType == DiscountType.TWO_FOR_PRICE)
                    {
                        result.Add(new AD_TwoForPrice(

                        this.productRepository.Get(
                            Convert.ToInt32(reader["ProductId"])),
                            Convert.ToDateTime(reader["StartDate"]),
                            Convert.ToDateTime(reader["EndDate"]),
                            Convert.ToDouble(reader["Price"])
                        ));
                    }
                    else if (discountType == DiscountType.SECOND_HALF_PRICE)
                    {
                        result.Add(new AD_SecondHalfPrice(

                        this.productRepository.Get(
                            Convert.ToInt32(reader["ProductId"])),
                            Convert.ToDateTime(reader["StartDate"]),
                            Convert.ToDateTime(reader["EndDate"])
                        ));
                    }
                    else if (discountType == DiscountType.TWO_FOR_ONE)
                    {
                        result.Add(new AD_TwoForOne(

                        this.productRepository.Get(
                            Convert.ToInt32(reader["ProductId"])),
                            Convert.ToDateTime(reader["StartDate"]),
                            Convert.ToDateTime(reader["EndDate"])
                        ));
                    }
                    else if (discountType == DiscountType.PRICE_FOR_WEIGHT)
                    {
                        result.Add(new AD_PriceForWeight(

                        this.productRepository.Get(
                            Convert.ToInt32(reader["ProductId"])),
                            Convert.ToDateTime(reader["StartDate"]),
                            Convert.ToDateTime(reader["EndDate"]),
                            Convert.ToDouble(reader["Price"]),
                            Convert.ToDouble(reader["Weight"]),
                            (Unit)(reader["Unit"]
                        )));
                    }
                }
                return result;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        /// <summary>
        /// Returns a list of all products that have a discount registered on the database
        /// </summary>
        /// <returns></returns>
        public List<int> GetDiscountedProductIds()
        {
            List<int> result = new List<int>();
            try
            {
                sqlConnection.Open();
                var command = new MySqlCommand("SELECT ProductId FROM `advertisements`;", sqlConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    result.Add(Convert.ToInt32(reader["ProductId"]));

                }
                return result;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        /// <summary>
        /// Removes an advertisement from the database
        /// </summary>
        /// <param name="id"></param>
        public void Remove(int id)
        {
            try
            {
                sqlConnection.Open();
                var command = new MySqlCommand("DELETE FROM `advertisements` WHERE ProductId=@ProductId;", sqlConnection);
                command.Parameters.AddWithValue("@ProductId", id);
                command.ExecuteNonQuery();
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }
}
