﻿using DomainLayer.Enums;
using DomainLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer
{
    [Serializable]
    public abstract class Advertisement : IProductDiscountAdvertisement
    {
        public Product Product { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }

        public Advertisement(Product product, DateTime startDate, DateTime endDate)
        {
            this.Product = product;
            this.StartDate = startDate;
            this.EndDate = endDate;
        }

        /// <summary>
        /// Returns the discount for the specified quantity of products, if there is an active advertisement. Each type of Advertisement has it's own way of calculating the discount.
        /// </summary>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public virtual double CalculateDiscount(int quantity)
        {
            double discount = 0;
            return discount;
        }

        /// <summary>
        /// Checks if the advertisement is active (Using dates).
        /// </summary>
        /// <returns></returns>
        public bool IsActive()
        {
            if (DateTime.Compare(this.StartDate, DateTime.Now) <= 0 && DateTime.Compare(this.EndDate, DateTime.Now) >= 0)
            {
                return true;
            }
            return false;
        }
    }
}
