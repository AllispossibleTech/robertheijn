﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer
{
    public class Employee : User
    {
        public DateTime ContractStart { get; private set; }
        public string Position { get; private set; }

        public Employee(string email, string password, string firstName, string surname, DateTime birthDate, Address address, string phoneNumber, DateTime contractStart, string position) : base(email, password, firstName, surname, birthDate, address, phoneNumber)
        {
            this.ContractStart = contractStart;
            this.Position = position;
        }

        public Employee(int id, string email, string password, string firstName, string surname, DateTime birthDate, Address address, string phoneNumber, DateTime contractStart, string position) : base(id, email, password, firstName, surname, birthDate, address, phoneNumber)
        {
            this.ContractStart = contractStart;
            this.Position = position;
        }
    }
}
