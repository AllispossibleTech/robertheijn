﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer
{
    public class PasswordHandler
    {
        private static string pepper = "theBestPepperintheWorld";
        public static string GetRandomSalt()
        {
            return BCrypt.Net.BCrypt.GenerateSalt(12);
        }
        public static string HashPassword(string password)
        {
            string pepperedPass = password + pepper;
            return BCrypt.Net.BCrypt.HashPassword(pepperedPass, GetRandomSalt());
        }
        public static bool ValidatePassword(string password, string correctHash)
        {
            {
                return BCrypt.Net.BCrypt.Verify(password + pepper, correctHash);
            }
        }
    }
}
