﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer
{
    public class OrderItem
    {
        public Product Product { get; private set; }
        public int Quantity { get; private set; }

        /// <summary>
        /// Calculated discount at the moment of order based on active advertisements
        /// </summary>
        public double Discount { get; private set; }

        public OrderItem(Product product, int quantity, double discount)
        {
            Product = product;
            Quantity = quantity;
            Discount = discount;
        }

        public double GetTotal()
        {
            return (this.Product.Price * this.Quantity) - this.Discount;
        }
    }
}
