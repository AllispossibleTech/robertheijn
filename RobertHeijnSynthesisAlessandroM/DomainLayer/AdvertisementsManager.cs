﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainLayer.Interfaces;
using DomainLayer.Enums;

namespace DomainLayer
{
    public class AdvertisementsManager
    {
        private IAdvertisementRepository advertisementRepository;
        public AdvertisementsManager(IAdvertisementRepository advertisementRepository)
        {
            this.advertisementRepository = advertisementRepository;
        }

        public List<IProductDiscountAdvertisement> GetAdvertisements()
        {
            return this.advertisementRepository.GetAll();
        }

        /// <summary>
        /// Checks if the input product has an active discount: using the dates.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool HasActiveDiscount(Product p)
        {
            if (this.advertisementRepository.GetDiscountedProductIds().Contains(p.Id))
            {
                if (this.advertisementRepository.Get(p.Id).IsActive())
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns a list of all Active advertisements
        /// </summary>
        /// <returns></returns>
        public List<Advertisement> GetActiveAdvertisements()
        {
            List<Advertisement> result = new List<Advertisement>();

            foreach (Advertisement a in this.advertisementRepository.GetAll())
            {
                if (a.IsActive())
                {
                    result.Add(a);
                }
            }
            return result;
        }

        /// <summary>
        /// Returns an existing advertisement object based on the product id associated. Be careful! It may also return a non active advertisment!
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public Advertisement GetAdvertisement(int productId)
        {
            foreach (Advertisement a in this.advertisementRepository.GetAll())
            {
                if (a.Product.Id == productId)
                {
                    return a;
                }
            }
            return null;
        }

        public void Add(IProductDiscountAdvertisement a)
        {
            if (this.HasActiveDiscount(((Advertisement)a).Product) == false)
            {
                this.advertisementRepository.Add(a);
            }
        }

        public void Remove(int id)
        {
            this.advertisementRepository.Remove(id);
        }
    }
}
