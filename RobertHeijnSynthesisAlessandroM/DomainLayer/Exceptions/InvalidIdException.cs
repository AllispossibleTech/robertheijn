﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Exceptions
{
    public class InvalidIdException : Exception
    {
        public InvalidIdException() : base("Provided ID does not match any registered user")
        {

        }
    }
}
