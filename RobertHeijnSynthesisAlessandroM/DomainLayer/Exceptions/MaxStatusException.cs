﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Exceptions
{
    public class MaxStatusException : Exception
    {
        public MaxStatusException() : base("Order Status has already reached it's final value")
        {

        }
    }
}
