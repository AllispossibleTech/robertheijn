﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace DomainLayer.Advertisements
{
    public class AD_TwoForPrice : Advertisement
    {
        public double Price { get; private set; }

        public AD_TwoForPrice(Product product, DateTime startDate, DateTime endDate, double price) : base(product, startDate, endDate)
        {
            this.Price = price;
        }

        public override double CalculateDiscount(int quantity)
        {
            if (quantity >= 2)
            {
                double normalPrice = this.Product.Price * quantity;
                return normalPrice - this.Price;
            }
            return 0;
        }
    }
}
