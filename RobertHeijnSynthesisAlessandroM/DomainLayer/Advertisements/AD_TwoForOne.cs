﻿using DomainLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Advertisements
{
    public class AD_TwoForOne : Advertisement
    {

        public AD_TwoForOne(Product product, DateTime startDate, DateTime endDate) : base(product, startDate, endDate)
        {

        }

        public override double CalculateDiscount(int quantity)
        {
            if (quantity >= 2)
            {
                return this.Product.Price;
            }
            return 0;
        }
    }
}
