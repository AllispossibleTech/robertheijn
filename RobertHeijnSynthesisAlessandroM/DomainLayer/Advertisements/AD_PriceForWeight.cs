﻿using DomainLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Advertisements
{
    public class AD_PriceForWeight : Advertisement
    {
        public double Price { get; private set; }
        public double Weight { get; private set; }
        public Unit Unit { get; private set; }

        public AD_PriceForWeight(Product product, DateTime startDate, DateTime endDate, double price, double weight, Unit unit) : base(product, startDate, endDate)
        {
            this.Price = price;
            this.Weight = weight;
            this.Unit = unit;
        }

        public override double CalculateDiscount(int quantity)
        {
            if (this.Product.Unit == this.Unit)
            {
                if (this.Product.Weight * quantity == this.Weight)
                {
                    double normalPrice = this.Product.Price * quantity;
                    return normalPrice - this.Price;
                }
            }
            return 0;
        }
    }
}
