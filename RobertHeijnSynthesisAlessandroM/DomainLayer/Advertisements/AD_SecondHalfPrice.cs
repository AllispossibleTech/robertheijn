﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Advertisements
{
    public class AD_SecondHalfPrice : Advertisement
    {
        public AD_SecondHalfPrice(Product product, DateTime startDate, DateTime endDate) : base(product, startDate, endDate)
        {
        }

        public override double CalculateDiscount(int quantity)
        {
            if (quantity >= 2)
            {
                double normalPrice = this.Product.Price * quantity;
                return normalPrice - (this.Product.Price + Math.Round((this.Product.Price / 2), 2));
            }
            return 0;
        }
    }
}
