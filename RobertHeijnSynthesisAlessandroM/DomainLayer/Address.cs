﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DomainLayer.Exceptions;

namespace DomainLayer
{
    public class Address
    {
        public string Street { get; private set; }
        public int HouseNumber { get; private set; }
        public string City { get; private set; }
        public string PostalCode { get; private set; }
        public string Country { get; private set; }

        /// <summary>
        /// Checks if provided postal code is valid with regular expression.
        /// </summary>
        /// <param name="street"></param>
        /// <param name="houseNumber"></param>
        /// <param name="city"></param>
        /// <param name="postalCode"></param>
        /// <param name="country"></param>
        /// <exception cref="InvalidPostalCodeException"></exception>
        public Address(string street, int houseNumber, string city, string postalCode, string country)
        {
            this.Street = street;
            this.HouseNumber = houseNumber;
            this.City = city;
            this.PostalCode = postalCode;
            this.Country = country;

            Regex regex = new Regex("^[1-9][0-9]{3}\\s?[a-zA-Z]{2}$");
            if (regex.IsMatch(postalCode))
            {
                this.PostalCode = postalCode;
            }
            else
            {
                throw new InvalidPostalCodeException();
            }
        }

        public string ToString()
        {
            return $"{this.Street} {this.HouseNumber}, {this.PostalCode} {this.City} - {this.Country}";
        }
    }
}
