﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Interfaces
{
    public interface IOrderRepository
    {
        List<Order> GetAll();
        void UpdateStatus(Order order);
        void Add(Order order);
        List<Order> GetCustomerOrders(int id);
    }
}
