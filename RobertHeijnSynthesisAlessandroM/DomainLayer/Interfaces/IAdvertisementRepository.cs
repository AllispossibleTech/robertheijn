﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Interfaces
{
    public interface IAdvertisementRepository
    {
        void Add(IProductDiscountAdvertisement advertisement);
        void Remove(int id);
        List<IProductDiscountAdvertisement> GetAll();
        IProductDiscountAdvertisement Get(int id);
        List<int> GetDiscountedProductIds();
    }
}
