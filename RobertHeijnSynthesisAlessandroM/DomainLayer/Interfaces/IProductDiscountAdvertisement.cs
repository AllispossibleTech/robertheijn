﻿using DomainLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Interfaces
{
    public interface IProductDiscountAdvertisement
    {
        double CalculateDiscount(int quantity);
        bool IsActive();
    }
}
