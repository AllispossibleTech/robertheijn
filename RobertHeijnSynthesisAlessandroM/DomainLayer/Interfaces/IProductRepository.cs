﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Interfaces
{
    public interface IProductRepository
    {
        void Add(Product product);
        void Remove(int productId);
        Product Get(int productId);
        List<Product> GetAll();
        int GetStockLevel(int productId);
    }
}
