﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Interfaces
{
    public interface IUserRepository
    {
        List<User> GetUsers();

        void Add(User user);

        void Delete(User user);

        bool Exists(User user);


    }
}
