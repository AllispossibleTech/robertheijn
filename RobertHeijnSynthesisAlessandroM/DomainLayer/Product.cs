﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using DomainLayer.Enums;
using DomainLayer.Interfaces;


namespace DomainLayer
{
    [Serializable]
    public class Product
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string SubCategory { get; private set; }
        public string Category { get; private set; }
        public double Price { get; private set; }
        public Unit Unit { get; private set; }
        public double Weight { get; private set; }

        // ToDo: Could have inheritance also for Products

        /// <summary>
        /// Constructor used when a new product is created from shop employees.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="subCategory"></param>
        /// <param name="category"></param>
        /// <param name="price"></param>
        /// <param name="unit"></param>
        public Product(string name, string subCategory, string category, double price, Unit unit)
        {
            this.Name = name;
            this.SubCategory = subCategory;
            this.Category = category;
            this.Price = price;
            this.Unit = unit;
        }

        public Product(string name, string subCategory, string category, double price, Unit unit, double weight)
        {
            this.Name = name;
            this.SubCategory = subCategory;
            this.Category = category;
            this.Price = price;
            this.Unit = unit;
            this.Weight = weight;
        }

        /// <summary>
        /// Constructor used when data is read from db.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="subCategory"></param>
        /// <param name="category"></param>
        /// <param name="price"></param>
        /// <param name="unit"></param>
        /// <param name="advertisementRepository"></param>
        [JsonConstructor]
        public Product(int id, string name, string subCategory, string category, double price, Unit unit)
        {
            this.Id = id;
            this.Name = name;
            this.SubCategory = subCategory;
            this.Category = category;
            this.Price = price;
            this.Unit = unit;
        }

        /// <summary>
        /// Constructor used when data is read from db and has a weight.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="subCategory"></param>
        /// <param name="category"></param>
        /// <param name="price"></param>
        /// <param name="unit"></param>
        /// <param name="weight"></param>
        public Product(int id, string name, string subCategory, string category, double price, Unit unit, double weight)
        {
            this.Id = id;
            this.Name = name;
            this.SubCategory = subCategory;
            this.Category = category;
            this.Price = price;
            this.Unit = unit;
            this.Weight = weight;
        }
    }
}
