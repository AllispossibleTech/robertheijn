﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer
{
    public class UserManager
    {
        Interfaces.IUserRepository userRepository;

        public UserManager(Interfaces.IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public List<User> GetUsers()
        {
            return this.userRepository.GetUsers();
        }

        /// <summary>
        /// Searches for a registered User based on the provided Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exceptions.InvalidIdException"></exception>
        public User GetUser(int id)
        {
            foreach (User u in this.GetUsers())
            {
                if (u.Id == id)
                {
                    return u;
                }
            }
            throw new Exceptions.InvalidIdException();
        }

        /// <summary>
        /// Checks if the provided customer has an order matching the provided id
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasOrder(Customer customer, int id)
        {
            foreach(Order o in customer.Orders)
            {
                if (o.Id == id)
                {
                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// This Method checks if the credentials provided match one entry of the registered User data and returns the user Id
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        /// <exception cref="Exceptions.InvalidCredentialsException"></exception>
        public int GetUserId(string email, string password)
        {
            foreach (User u in this.GetUsers())
            {
                if (u.Email == email && PasswordHandler.ValidatePassword(password, u.Password) == true)
                {
                    return u.Id;
                }
            }
            // If credentials do not match, an appropiate exception is thrown.
            throw new Exceptions.InvalidCredentialsException();
        }

        public void Add(User user)
        {
            // Password passed to here will already be hashed using the PasswordHandler.HashPassword() in the PresentationLayers
            this.userRepository.Add(user);
            // ToDo: I should add logic to check if the user already exists!
        }
    }
}
