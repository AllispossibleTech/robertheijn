﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainLayer.Enums;
using DomainLayer.Exceptions;
using DomainLayer.Interfaces;

namespace DomainLayer
{
    public class Order
    {
        public int Id { get; private set; }
        public Customer Customer { get; private set; }
        public List<OrderItem> OrderItems { get; private set; }
        public DateTime OrderDate { get; private set; }
        public DateTime DeliveryDate { get; private set; }
        public OrderStatus OrderStatus { get; private set; }
        public PaymentMethod PaymentMethod { get; private set; }

        public Order(ShoppingCart shoppingCart, PaymentMethod paymentMethod, Customer customer)
        {
            this.Customer = customer;
            this.OrderDate = DateTime.Now;
            this.OrderStatus = OrderStatus.IN_PREPARATION;
            // ToDo: don't know how many days it takes for the shipment to be delivered
            this.DeliveryDate = DateTime.Now.AddDays(7);
            this.PaymentMethod = paymentMethod;
            this.OrderItems = new List<OrderItem>();

            foreach(CartItem c in shoppingCart.Products)
            {
                this.OrderItems.Add(new OrderItem(c.Product, c.Quantity, c.GetDiscount()));
            }
        }

        public Order(int id, DateTime orderDate, DateTime deliveryDate, OrderStatus orderStatus, PaymentMethod paymentMethod)
        {
            this.Id = id;
            this.OrderDate = orderDate;
            this.DeliveryDate = deliveryDate;
            this.OrderStatus = orderStatus;
            this.PaymentMethod = paymentMethod;
            this.OrderItems = new List<OrderItem>();

            // ToDo: I could also store billing info and check with a regex the validity of it.
        }

        public double GetTotal()
        {
            double result = 0;
            foreach(OrderItem o in this.OrderItems)
            {
                result += o.GetTotal();
            }
            return result;
        }

        public void Add(OrderItem orderItem)
        {
            this.OrderItems.Add(orderItem);
        }

        public void AdvanceStatus()
        {
            if (this.OrderStatus < OrderStatus.SHIPPED)
            {
                this.OrderStatus = this.OrderStatus + 1;
            }
            else
            {
                throw new MaxStatusException();
            }
        }
    }
}
