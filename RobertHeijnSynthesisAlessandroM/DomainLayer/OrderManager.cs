﻿using DomainLayer.Interfaces;
using DomainLayer.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainLayer.Enums;

namespace DomainLayer
{
    public class OrderManager
    {

        // ToDo: I had to create an order manager class because the Customer class can be instantiated from the DataAccessLayer: i cannot pass an OrderRepository object to the customer object inside an UserRepository object.

        private IOrderRepository orderRepository;

        public OrderManager(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }
        
        public void AddOrder(Order order)
        {
            this.orderRepository.Add(order);
        }

        public void AdvanceStatus(Order order)
        {
            if (order.OrderStatus < OrderStatus.SHIPPED)
            {
                this.orderRepository.UpdateStatus(order);
            }
            else
            {
                throw new MaxStatusException();
            }
        }

        public Order GetOrder(int id)
        {
            foreach (Order o in this.orderRepository.GetAll())
            {
                if (o.Id == id)
                { 
                    return o; 
                }
            }
            throw new OrderNotFoundException();
        }

        public Order GetOrderByCustomerId(int id)
        {
            foreach (Order o in this.orderRepository.GetAll())
            {
                if (o.Customer.Id == id)
                {
                    return o;
                }
            }
            throw new OrderNotFoundException();
        }
    }
}
