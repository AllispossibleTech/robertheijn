﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer
{
    public abstract class User
    {

        public int Id { get; private set; }
        public string FirstName { get; private set; }
        public string Surname { get; private set; }
        public DateTime BirthDate { get; private set; }
        public Address Address { get; private set; }
        public string PhoneNumber { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }


        public User (string email, string password, string firstName, string surname, DateTime birthDate, Address address, string phoneNumber)
        {
            this.FirstName = firstName;
            this.Surname = surname;
            this.BirthDate = birthDate;
            this.Address = address;
            this.PhoneNumber = phoneNumber;
            this.Email = email;
            this.Password = password;
        }

        public User(int id, string email, string password, string firstName, string surname, DateTime birthDate, Address address, string phoneNumber)
        {
            this.Id = id;
            this.Email = email;
            this.Password = password;
            this.FirstName = firstName;
            this.Surname = surname;
            this.BirthDate = birthDate;
            this.Address = address;
            this.PhoneNumber = phoneNumber;
        }

        public int GetAge()
        {
            DateTime zeroTime = new DateTime(1, 1, 1);
            TimeSpan span = DateTime.Now - this.BirthDate;
            int years = (zeroTime + span).Year - 1;
            return years;
        }
    }
}
