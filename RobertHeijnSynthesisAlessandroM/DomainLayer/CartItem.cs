﻿using DomainLayer.Enums;
using DomainLayer.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer
{
    [Serializable]
    public class CartItem
    {
        public int Id { get; private set; }
        public Product Product { get; private set; }
        public int Quantity { get; private set; }

        private IAdvertisementRepository advertisementRepository;

        private IProductDiscountAdvertisement productDiscountAdvertisement;

        public CartItem(Product product, int quantity, IAdvertisementRepository advertisementRepository)
        { 
            this.Product = product;
            this.Quantity = quantity;
            this.Id = product.Id;
            this.advertisementRepository = advertisementRepository;
        }

        public bool HasActiveDiscount()
        {
            if (this.advertisementRepository.GetDiscountedProductIds().Contains(this.Product.Id))
            {
                Advertisement a = (Advertisement)this.advertisementRepository.Get(this.Product.Id);
                bool isActive = a.IsActive();
                if (((Advertisement)this.advertisementRepository.Get(this.Product.Id)).IsActive())
                {
                    return true;
                }
            }
            return false;
        }


        // ToDo: some parts of the code that is being called are then repeated again in this method so it does not have any sense: too many DB accesses for nothing. Can be improved by doing everything in 1 method.
        public double GetDiscount()
        {
            double result = 0;
            if (this.HasActiveDiscount())
            {
                this.productDiscountAdvertisement = this.advertisementRepository.Get(this.Product.Id);
                result = productDiscountAdvertisement.CalculateDiscount(this.Quantity);
            }
            return result;
        }

        public void EditQuantity(int quantity)
        {
            if (quantity > 0)
            {
                this.Quantity = quantity;
            }
        }

        public double GetWeight()
        {
            if (this.Product.Unit == Unit.KILO && this.Product.Unit == Unit.GRAM)
            {
                return (this.Product.Weight * this.Quantity);
            }
            return -1;
        }

        public double GetTotal()
        {
            return (this.Product.Price * this.Quantity) - this.GetDiscount();
        }

        public void UseRepository(IAdvertisementRepository advertisementRepository)
        {
            this.advertisementRepository = advertisementRepository;
        }
    }
}
