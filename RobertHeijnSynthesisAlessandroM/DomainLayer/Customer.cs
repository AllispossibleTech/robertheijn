﻿namespace DomainLayer
{
    public class Customer : User
    {

        public BonusCard BonusCard { get; private set; }
        public List<Order> Orders { get; private set; }

        public Customer(string email, string password, string firstName, string surname, DateTime birthDate, Address address, string phoneNumber) : base (email, password, firstName, surname, birthDate, address, phoneNumber)
        {
            this.BonusCard = new BonusCard();
            // ToDo: THIS WILL GO IN THE PRESENTATION LAYER !!!!
            // this.ShoppingCart = new ShoppingCart(this.BonusCard);
            this.Orders = new List<Order>();
        }

        public Customer(int id, string email, string password, string firstName, string surname, DateTime birthDate, Address address, string phoneNumber, BonusCard bonusCard, List<Order> orders) : base (id, email, password, firstName, surname, birthDate, address, phoneNumber)
        {
            this.BonusCard = bonusCard;
            this.Orders = orders;
        }

        public Order GetOrder(int id)
        {
            foreach (Order o in this.Orders)
            { 
                if (o.Id == id)
                {
                    return o;
                }
            }
            return null;
        }

        public void AddOrder(Order order)
        {
            foreach (Order o in this.Orders)
            {
                if (o.Id == order.Id)
                {
                    return;
                }
            }
            this.Orders.Add(order);
        }

    }
}