﻿using DomainLayer.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer
{
    [Serializable]
    public class ShoppingCart
    {
        // ALL CART ITEMS PASSED TO THIS CLASS WILL RETRIEVE THEIR DISCOUNT ON THEIR OWN

        public List<CartItem> Products { get; private set; }

        private IAdvertisementRepository advertisementRepository { get; set; }

        /// <summary>
        /// Class that is responsible for saving the products of the cart.
        /// </summary>
        /// <param name="advertisementRepository"></param>
        public ShoppingCart(IAdvertisementRepository advertisementRepository)
        {
            this.Products = new List<CartItem>();
            this.advertisementRepository = advertisementRepository;
        }

        /// <summary>
        /// Constructor used for Deserialization
        /// </summary>
        /// <param name="advertisementRepository"></param>
        /// <param name="products"></param>
        [JsonConstructor]
        public ShoppingCart(IAdvertisementRepository advertisementRepository, List<CartItem> Products)
        {
            this.advertisementRepository = advertisementRepository;
            this.Products = Products;
        }

        /// <summary>
        /// Adds a product to the cart.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="quantity"></param>
        public void Add(Product product, int quantity)
        {
            Products.Add(new CartItem(product, quantity, this.advertisementRepository));
        }

        /// <summary>
        /// Edits the quantity of a product for a certain item in the cart.
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="quantity"></param>
        public void EditQuantity(int cartItemId, int quantity)
        {
            if (quantity > 0)
            {
                foreach (CartItem c in this.Products)
                {
                    if (c.Id == cartItemId)
                    {
                        c.EditQuantity(quantity);
                    }
                }
            }
            else if (quantity == 0)
            {
                this.Remove(cartItemId);
            }
        }

        /// <summary>
        /// Removes an item from the cart.
        /// </summary>
        /// <param name="id"></param>
        public void Remove(int id)
        {
            foreach(CartItem c in this.Products)
            {
                if (c.Id == id)
                {
                    this.Products.Remove(c);
                    break;
                }
            }
        }

        /// <summary>
        /// Calculates the total price of the items in the cart, discounts included.
        /// </summary>
        /// <param name="bonusCard"></param>
        /// <returns></returns>
        public double GetTotal(BonusCard bonusCard)
        {
            double total = 0;
            foreach (CartItem c in this.Products)
            {
                total += c.GetTotal();
            }
            if (bonusCard.HasDiscount())
            {
                total -= bonusCard.GetDiscount();
            }
            return total;
        }

        /// <summary>
        /// To be used every time the shopping cart object is deserialized from a session.
        /// </summary>
        /// <param name="advertisementRepository"></param>
        public void UseRepository(IAdvertisementRepository advertisementRepository)
        {
            this.advertisementRepository = advertisementRepository;
            foreach(CartItem c in this.Products)
            {
                c.UseRepository(advertisementRepository);
            }
        }

        public CartItem Get(int cartItemId)
        {
            foreach (CartItem c in this.Products)
            {
                if (c.Id == cartItemId)
                {
                    return c;
                }
            }
            throw new Exception("CartItem not found!");
        }
    }
}
