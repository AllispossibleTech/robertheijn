﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainLayer.Interfaces;

namespace DomainLayer
{
    public class Stock
    {
        IProductRepository productRepository;
        IAdvertisementRepository advertisementRepository;

        public Stock(IProductRepository productRepository, IAdvertisementRepository advertisementRepository)
        {
            this.productRepository = productRepository;
            this.advertisementRepository = advertisementRepository;
        }

        public List<Product> GetProducts()
        {
            return this.productRepository.GetAll();
        }

        public Product GetProduct(int id)
        {
            foreach (Product product in this.GetProducts())
            {
                if (product.Id == id)
                {
                    return product;
                }
            }
            return null;
        }

        public bool Exists(int id)
        {
            foreach (Product p in this.GetProducts())
            {
                if (p.Id == id)
                {
                    return true;
                }
            }
            return false;
        }

        public void Add(Product product)
        {
            if (!this.Exists(product.Id))
            {
                this.productRepository.Add(product);
            }
        }

        public void Remove(int id)
        {
            if (this.Exists(id))
            {
                this.productRepository.Remove(id);
            }
        }

        public List<Product> GetProductsWithActiveDiscount()
        {
            List<Product> result = new List<Product>();
            foreach (Advertisement a in this.advertisementRepository.GetAll())
            {
                if(a.IsActive() && this.Exists(a.Product.Id))
                {
                    result.Add(a.Product);
                }
            }
            return result;
        }

    }
}
