﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer
{
    public class BonusCard
    {
        public int Id { get; private set; }
        public int Points { get; private set; }

        public BonusCard()
        {
            this.Points = 0;
        }

        public BonusCard(int id, int points)
        {
            this.Id = id;
            this.Points = points;
        }

        public void AddPoints(int points)
        {
            // This method has no sense in this context, could be used in the future
            this.Points += points;
        }

        /// <summary>
        /// Discount is available if BonusCard has at least 100 points
        /// </summary>
        /// <returns></returns>
        public bool HasDiscount()
        {
            int euroDiscount = this.Points / 100;
            return (euroDiscount > 0);
        }

        public int GetDiscount()
        {
            int euroDiscount = this.Points / 100;
            return euroDiscount;
        }

    }
}
