﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Enums
{
    public enum DiscountType
    {
        TWO_FOR_ONE,
        TWO_FOR_PRICE,
        PRICE_FOR_WEIGHT,
        SECOND_HALF_PRICE
    }
}
