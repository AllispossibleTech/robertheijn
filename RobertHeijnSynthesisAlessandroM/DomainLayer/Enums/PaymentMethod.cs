﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Enums
{
    public enum PaymentMethod
    {
        CREDIT_CARD,
        DEBIT_CARD,
        PAYPAL,
        GOOGLE_PAY,
        APPLE_PAY
    }
}
