﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Enums
{
    public enum OrderStatus
    {
        IN_PREPARATION,
        AWAITING_SHIPMENT,
        SHIPPED,
        DELIVERED
    }
}
