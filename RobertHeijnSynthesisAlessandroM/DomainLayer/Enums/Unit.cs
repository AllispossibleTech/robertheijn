﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Enums
{
    public enum Unit
    {
        NO_UNIT,
        KILO,
        GRAM
    }
}
