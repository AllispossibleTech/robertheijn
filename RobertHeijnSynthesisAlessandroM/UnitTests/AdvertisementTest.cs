﻿using DomainLayer;
using DomainLayer.Advertisements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTests.MockRepositories;

namespace UnitTests
{
    [TestClass]
    public class AdvertisementTest
    {
        [TestMethod]
        public void IsActiveTest()
        {
            // Arrange
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            Advertisement advertisement = new AD_TwoForOne(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7));

            // Act
            bool result = advertisement.IsActive();

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotActiveTest()
        {
            // Arrange
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            Advertisement advertisement = new AD_TwoForOne(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(-2));

            // Act
            bool result = advertisement.IsActive();

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void AD_PriceForWeight_CalculateDiscount_Test()
        {
            // Arrange
            Product product1 = new Product(1, "Bread", "Baked", "Food", 6, DomainLayer.Enums.Unit.GRAM, 500);
            AD_PriceForWeight advertisement = new AD_PriceForWeight(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7), 3, 500, DomainLayer.Enums.Unit.GRAM);

            // Act
            double result = advertisement.CalculateDiscount(1);

            // Assert
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void AD_PriceForWeight_CalculateDiscount_2xQuantity_Test()
        {
            // Arrange
            Product product1 = new Product(1, "Bread", "Baked", "Food", 6, DomainLayer.Enums.Unit.GRAM, 250);
            AD_PriceForWeight advertisement = new AD_PriceForWeight(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7), 3, 500, DomainLayer.Enums.Unit.GRAM);

            // Act
            double result = advertisement.CalculateDiscount(2);

            // Assert
            Assert.AreEqual(9, result);
        }

        [TestMethod]
        public void AD_PriceForWeight_CalculateDiscount_UnitDoesNotMatch_Test()
        {
            // Arrange
            Product product1 = new Product(1, "Bread", "Baked", "Food", 6, DomainLayer.Enums.Unit.KILO, 1);
            AD_PriceForWeight advertisement = new AD_PriceForWeight(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7), 3, 500, DomainLayer.Enums.Unit.GRAM);

            // Act
            double result = advertisement.CalculateDiscount(1);

            // Assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void AD_TwoForPrice_CalculateDiscount_Test()
        {
            // Arrange
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            AD_TwoForPrice advertisement = new AD_TwoForPrice(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7), 2);

            // Act
            double result = advertisement.CalculateDiscount(2);

            // Assert
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void AD_TwoForOne_CalculateDiscount_Test()
        {
            // Arrange
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            AD_TwoForOne advertisement = new AD_TwoForOne(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7));

            // Act
            double result = advertisement.CalculateDiscount(2);

            // Assert
            Assert.AreEqual(1.50, result);
        }

        [TestMethod]
        public void AD_SecondHalfPrice_CalculateDiscount_Test()
        {
            // Arrange
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 2, DomainLayer.Enums.Unit.NO_UNIT);
            AD_SecondHalfPrice advertisement = new AD_SecondHalfPrice(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7));

            // Act
            double result = advertisement.CalculateDiscount(2);

            // Assert
            Assert.AreEqual(1, result);
        }

    }
}
