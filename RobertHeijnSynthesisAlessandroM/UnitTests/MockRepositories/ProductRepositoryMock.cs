﻿using DomainLayer;
using DomainLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.MockRepositories
{
    public class ProductRepositoryMock : IProductRepository
    {
        private List<Product> products;
        public ProductRepositoryMock()
        {
            this.products = new List<Product>();
        }
        public void Add(Product product)
        {
            this.products.Add(product);
        }

        public Product Get(int productId)
        {
            foreach(Product p in this.products)
            {
                if (p.Id == productId)
                {
                    return p;
                }
            }
            throw new Exception("Product not found");
        }

        public List<Product> GetAll()
        {
            return this.products;
        }

        public int GetStockLevel(int productId)
        {
            throw new NotImplementedException();
        }

        public void Remove(int productId)
        {
            foreach (Product p in this.products)
            {
                if (p.Id == productId)
                {
                    products.Remove(p);
                }
            }
            throw new Exception("Product not found");
        }
    }
}
