﻿using DomainLayer;
using DomainLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.MockRepositories
{
    public class OrderRepositoryMock : IOrderRepository
    {
        private List<Order> orders;

        public OrderRepositoryMock()
        {
            this.orders = new List<Order>();
        }
        public void Add(Order order)
        {
            this.orders.Add(order);
        }

        public List<Order> GetAll()
        {
            return this.orders;
        }

        public List<Order> GetCustomerOrders(int id)
        {
            List<Order> result = new List<Order>();
            foreach (Order order in this.orders)
            {
                if (order.Customer.Id == id)
                {
                    result.Add(order);
                }
            }
            return result;
        }

        public void UpdateStatus(Order order)
        {
            foreach (Order orderI in this.orders)
            {
                if (orderI.Id == order.Id)
                {
                    orderI.AdvanceStatus();
                }
            }
        }
    }
}
