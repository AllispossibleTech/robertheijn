﻿using DomainLayer;
using DomainLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.MockRepositories
{
    public class UserRepositoryMock : IUserRepository
    {
        private List<User> users;
        public UserRepositoryMock()
        {
             this.users = new List<User>();
        }
        public void Add(User user)
        {
            this.users.Add(user);
        }

        public void Delete(User user)
        {
            if (this.Exists(user))
            {
                this.users.Remove(user);
            }
        }

        public bool Exists(User user)
        {
            if (this.users.Contains(user))
            {
                return true;
            }
            return false;
        }

        public List<User> GetUsers()
        {
            return this.users;
        }
    }
}
