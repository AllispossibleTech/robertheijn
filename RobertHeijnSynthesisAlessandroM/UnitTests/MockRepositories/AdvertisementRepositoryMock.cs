﻿using DomainLayer.Interfaces;
using DomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.MockRepositories
{
    public class AdvertisementRepositoryMock : IAdvertisementRepository
    {
        private List<IProductDiscountAdvertisement> advertisements;

        public AdvertisementRepositoryMock()
        {
            this.advertisements = new List<IProductDiscountAdvertisement>();
        }

        public void Add(IProductDiscountAdvertisement advertisement)
        {
            this.advertisements.Add(advertisement);
        }

        public IProductDiscountAdvertisement Get(int id)
        {
            foreach (IProductDiscountAdvertisement advertisement in this.advertisements)
            {
                if (((Advertisement)advertisement).Product.Id == id)
                {
                    return advertisement;
                }
            }
            throw new Exception("Adv not found");
        }

        public List<IProductDiscountAdvertisement> GetAll()
        {
            return this.advertisements;
        }

        public List<int> GetDiscountedProductIds()
        {
            List<int> result = new List<int>();

            foreach (IProductDiscountAdvertisement advertisement in this.advertisements)
            {
                result.Add(((Advertisement)advertisement).Product.Id);
            }
            return result;
        }

        public void Remove(int id)
        {
            foreach (IProductDiscountAdvertisement advertisement in this.advertisements)
            {
                if (((Advertisement)advertisement).Product.Id == id)
                {
                    this.advertisements.Remove(advertisement);
                }
            }
            throw new Exception("Adv not found");
        }
    }
}
