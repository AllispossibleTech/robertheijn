﻿using DomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTests.MockRepositories;

namespace UnitTests
{
    [TestClass]
    public class BonusCardTest
    {
        [TestMethod]
        public void HasDiscountTest()
        {
            // Arrange
            BonusCard bonusCard = new BonusCard(1, 456);

            // Act
            bool result = bonusCard.HasDiscount();

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void HasNoDiscountTest()
        {
            // Arrange
            BonusCard bonusCard = new BonusCard(1, 56);

            // Act
            bool result = bonusCard.HasDiscount();

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GetDiscountTest()
        {
            // Arrange
            BonusCard bonusCard = new BonusCard(1, 456);

            // Act
            double result = bonusCard.GetDiscount();

            // Assert
            Assert.AreEqual(4, result);
        }
    }
}
