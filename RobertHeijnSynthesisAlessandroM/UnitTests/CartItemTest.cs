﻿using DomainLayer.Advertisements;
using DomainLayer;
using DomainLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTests.MockRepositories;

namespace UnitTests
{
    [TestClass]
    public class CartItemTest
    {
        [TestMethod]
        public void HasDiscountTest()
        {
            // Arrange
            IAdvertisementRepository advertisementRepository = new AdvertisementRepositoryMock();
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            AD_TwoForOne advertisement = new AD_TwoForOne(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7));
            advertisementRepository.Add(advertisement);
            CartItem cartItem = new CartItem(product1, 2, advertisementRepository);

            // Act
            bool result = cartItem.HasActiveDiscount();

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void HasNoDiscountTest()
        {
            // Arrange
            IAdvertisementRepository advertisementRepository = new AdvertisementRepositoryMock();
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            Product product2 = new Product(2, "Bread", "Baked", "Food", 2, DomainLayer.Enums.Unit.GRAM, 200);
            AD_TwoForOne advertisement = new AD_TwoForOne(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7));
            advertisementRepository.Add(advertisement);
            CartItem cartItem = new CartItem(product2, 2, advertisementRepository);

            // Act
            bool result = cartItem.HasActiveDiscount();

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GetDiscountTest()
        {
            // Arrange
            IAdvertisementRepository advertisementRepository = new AdvertisementRepositoryMock();
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            AD_TwoForOne advertisement = new AD_TwoForOne(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7));
            advertisementRepository.Add(advertisement);
            CartItem cartItem = new CartItem(product1, 2, advertisementRepository);

            // Act
            double result = cartItem.GetDiscount();

            // Assert
            Assert.AreEqual(1.50, result);
        }

        [TestMethod]
        public void GetTotalTest()
        {
            // Arrange
            IAdvertisementRepository advertisementRepository = new AdvertisementRepositoryMock();
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            AD_TwoForOne advertisement = new AD_TwoForOne(product1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7));
            advertisementRepository.Add(advertisement);
            CartItem cartItem = new CartItem(product1, 3, advertisementRepository);

            // Act
            double result = cartItem.GetTotal();

            // Assert
            Assert.AreEqual(3, result);
        }

    }
}
