﻿using DomainLayer.Interfaces;
using DomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTests.MockRepositories;

namespace UnitTests
{
    [TestClass]
    public class UserManagerTest
    {
        [TestMethod]
        public void AddAndGetUsersTest()
        {
            // Arrange
            UserManager userManager = new UserManager(new UserRepositoryMock());
            Customer customer = new Customer(1, "am@email.com", "unhashedpassword", "Alessandro", "Mattiuzzo", DateTime.Now.AddYears(-18), new Address("Via Garibaldi", 10, "Ochiobello", "1234AB", "Italy"), "83745683834", new BonusCard(1, 0), new List<Order>());

            // Act
            userManager.Add(customer);
            Customer result = (Customer)userManager.GetUsers()[0];

            // Assert
            Assert.AreEqual(customer, result);
        }

        [TestMethod]
        public void GetUserTest()
        {
            // Arrange
            UserManager userManager = new UserManager(new UserRepositoryMock());
            Customer customer = new Customer(1, "am@email.com", "unhashedpassword", "Alessandro", "Mattiuzzo", DateTime.Now.AddYears(-18), new Address("Via Garibaldi", 10, "Ochiobello", "1234AB", "Italy"), "83745683834", new BonusCard(1, 0), new List<Order>());
            userManager.Add(customer);

            // Act
            Customer result = (Customer)userManager.GetUser(customer.Id);

            // Assert
            Assert.AreEqual(customer, result);
        }

        [TestMethod]
        public void HasOrderTest()
        {
            // Arrange
            UserManager userManager = new UserManager(new UserRepositoryMock());
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Customer customer = new Customer(1, "am@email.com", "unhashedpassword", "Alessandro", "Mattiuzzo", DateTime.Now.AddYears(-18), new Address("Via Garibaldi", 10, "Ochiobello", "1234AB", "Italy"), "83745683834", new BonusCard(1, 0), new List<Order>());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            shoppingCart.Add(product1, 2);
            customer.AddOrder(new Order(shoppingCart, DomainLayer.Enums.PaymentMethod.APPLE_PAY, customer));

            // Act
            bool result = userManager.HasOrder(customer, 0);

            // Assert
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void GetUserIdTest()
        {
            // Arrange
            UserManager userManager = new UserManager(new UserRepositoryMock());
            string hashed = PasswordHandler.HashPassword("unhashedpassword");
            Customer customer = new Customer(1, "am@email.com", hashed, "Alessandro", "Mattiuzzo", DateTime.Now.AddYears(-18), new Address("Via Garibaldi", 10, "Ochiobello", "1234AB", "Italy"), "83745683834", new BonusCard(1, 0), new List<Order>());
            userManager.Add(customer);

            // Act
            int result = userManager.GetUserId("am@email.com", "unhashedpassword");

            // Assert
            Assert.AreEqual(1, result);
        }



    }
}
