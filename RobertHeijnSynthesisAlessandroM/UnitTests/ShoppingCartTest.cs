using UnitTests.MockRepositories;
using DomainLayer;
using DomainLayer.Interfaces;

namespace UnitTests
{
    [TestClass]
    public class ShoppingCartTest
    {
        [TestMethod]
        public void AddAndGetCartItemsTest()
        {
            // Arrange
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            Product product2 = new Product(2, "Bread", "Baked", "Food", 2, DomainLayer.Enums.Unit.GRAM, 200);

            // Act
            shoppingCart.Add(product1, 2);
            shoppingCart.Add(product2, 1);
            int result = shoppingCart.Products.Count;

            // Assert
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void GetAndEditQuantityTest()
        {
            // Arrange
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);

            // Act
            shoppingCart.Add(product1, 2);
            shoppingCart.EditQuantity(1, 1);
            int result = shoppingCart.Get(1).Quantity;

            // Assert
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void EditQuantityToZeroTest()
        {
            // Arrange
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);

            // Act
            shoppingCart.Add(product1, 2);
            shoppingCart.EditQuantity(1, 0);
            int result = shoppingCart.Products.Count;

            // Assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void GetIncorrectCartItemIdTest()
        {
            // Arrange
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            string result = "";

            // Act
            try
            {
                shoppingCart.Get(1);
            }
            catch(Exception e)
            {
                result = e.Message;
            }

            // Assert
            Assert.AreEqual("CartItem not found!", result);
        }

        [TestMethod]
        public void GetTotalWithNoBonusCardPointsTest()
        {
            // Arrange
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            Product product2 = new Product(2, "Bread", "Baked", "Food", 2, DomainLayer.Enums.Unit.GRAM, 200);
            BonusCard bonusCard = new BonusCard(1, 0);
            shoppingCart.Add(product1, 2);
            shoppingCart.Add(product2, 1);

            // Act
            double result = shoppingCart.GetTotal(bonusCard);

            // Assert
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void GetTotalWithBonusCardPointsTest()
        {
            // Arrange
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            Product product2 = new Product(2, "Bread", "Baked", "Food", 2, DomainLayer.Enums.Unit.GRAM, 200);
            BonusCard bonusCard = new BonusCard(1, 456);
            shoppingCart.Add(product1, 2);
            shoppingCart.Add(product2, 1);

            // Act
            double result = shoppingCart.GetTotal(bonusCard);

            // Assert
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void UseRepositoryTest()
        {
            // Arrange
            ShoppingCart shoppingCart = new ShoppingCart(null);
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            BonusCard bonusCard = new BonusCard(1, 0);
            shoppingCart.Add(product1, 2);

            // Act
            shoppingCart.UseRepository(new AdvertisementRepositoryMock());
            double result = shoppingCart.GetTotal(bonusCard);

            // Assert
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void RemoveTest()
        {
            // Arrange
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            shoppingCart.Add(product1, 1);

            // Act
            shoppingCart.Remove(1);
            int result = shoppingCart.Products.Count;

            // Assert
            Assert.AreEqual(0, result);
        }
    }
}