﻿using DomainLayer;
using DomainLayer.Interfaces;
using DomainLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTests.MockRepositories;
using DomainLayer.Exceptions;

namespace UnitTests
{
    [TestClass]
    public class OrderManagerTest
    {
        [TestMethod]
        public void AddAndGetOrdersTest()
        {
            // Arrange
            IOrderRepository orderRepository = new OrderRepositoryMock();
            OrderManager orderManager = new OrderManager(orderRepository);
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Customer customer = new Customer(1, "am@email.com", "unhashedpassword", "Alessandro", "Mattiuzzo", DateTime.Now.AddYears(-18), new Address("Via Garibaldi", 10, "Ochiobello", "1234AB", "Italy"), "83745683834", new BonusCard(1, 0), new List<Order>());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, DomainLayer.Enums.Unit.NO_UNIT);
            Product product2 = new Product(2, "Bread", "Baked", "Food", 2, DomainLayer.Enums.Unit.GRAM, 200);
            shoppingCart.Add(product1, 2);
            shoppingCart.Add(product2, 1);

            // Act
            orderManager.AddOrder(new Order(shoppingCart, DomainLayer.Enums.PaymentMethod.APPLE_PAY, customer));
            int result = orderManager.GetOrderByCustomerId(customer.Id).OrderItems.Count;

            // Assert
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void AdvanceStatusTest()
        {
            // Arrange
            IOrderRepository orderRepository = new OrderRepositoryMock();
            OrderManager orderManager = new OrderManager(orderRepository);
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Customer customer = new Customer(1, "am@email.com", "unhashedpassword", "Alessandro", "Mattiuzzo", DateTime.Now.AddYears(-18), new Address("Via Garibaldi", 10, "Ochiobello", "1234AB", "Italy"), "83745683834", new BonusCard(1, 0), new List<Order>());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, Unit.NO_UNIT);
            Product product2 = new Product(2, "Bread", "Baked", "Food", 2, Unit.GRAM, 200);
            shoppingCart.Add(product1, 2);
            shoppingCart.Add(product2, 1);
            orderManager.AddOrder(new Order(shoppingCart, PaymentMethod.APPLE_PAY, customer));
            Order order = orderManager.GetOrderByCustomerId(customer.Id);

            // Act
            orderManager.AdvanceStatus(order);
            OrderStatus result = orderManager.GetOrderByCustomerId(customer.Id).OrderStatus;

            // Assert
            Assert.AreEqual(OrderStatus.AWAITING_SHIPMENT, result);
        }

        [TestMethod]
        public void AdvanceStatus_AdvancingMoreThanPermitted_Test()
        {
            // Arrange
            string result = "";
            IOrderRepository orderRepository = new OrderRepositoryMock();
            OrderManager orderManager = new OrderManager(orderRepository);
            ShoppingCart shoppingCart = new ShoppingCart(new AdvertisementRepositoryMock());
            Customer customer = new Customer(1, "am@email.com", "unhashedpassword", "Alessandro", "Mattiuzzo", DateTime.Now.AddYears(-18), new Address("Via Garibaldi", 10, "Ochiobello", "1234AB", "Italy"), "83745683834", new BonusCard(1, 0), new List<Order>());
            Product product1 = new Product(1, "Milk", "Drinkable", "Food", 1.50, Unit.NO_UNIT);
            Product product2 = new Product(2, "Bread", "Baked", "Food", 2, Unit.GRAM, 200);
            shoppingCart.Add(product1, 2);
            shoppingCart.Add(product2, 1);
            orderManager.AddOrder(new Order(shoppingCart, PaymentMethod.APPLE_PAY, customer));
            orderManager.GetOrderByCustomerId(customer.Id).AdvanceStatus();
            orderManager.GetOrderByCustomerId(customer.Id).AdvanceStatus();
            Order order = orderManager.GetOrderByCustomerId(customer.Id);

            // Act
            try
            {
                orderManager.AdvanceStatus(order);
            }
            catch(MaxStatusException e)
            {
                result = e.Message;
            }

            // Assert
            Assert.AreEqual("Order Status has already reached it's final value", result);
            // Assert.ThrowsException<MaxStatusException>(() => orderManager.GetOrderByCustomerId(customer.Id).AdvanceStatus());
        }
    }
}
