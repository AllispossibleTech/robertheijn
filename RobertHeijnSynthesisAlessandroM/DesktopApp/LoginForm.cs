using System.Runtime.InteropServices;
using DataAccessLayer;
using DomainLayer;
using DomainLayer.Exceptions;

namespace DesktopApp
{
    public partial class LoginForm : Form
    {
        UserManager userManager;
        public LoginForm()
        {
            InitializeComponent();
            userManager = new UserManager(new UserRepository());
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            int loggingId = 0;

            try
            {
                loggingId = this.userManager.GetUserId(txt_email.Text, txt_password.Text);
                
                if (this.userManager.GetUser(loggingId) is Employee)
                {
                    HomeFormEmp homeForm = new HomeFormEmp();
                    this.Hide();
                    homeForm.ShowDialog();
                    this.Show();
                }
                else
                {
                    lbl_error.Text = "Error: Unauthorized User!";
                    lbl_error.Visible = true;
                }
            }
            catch(Exception error)
            {
                lbl_error.Text = "Error: " + error.Message;
                lbl_error.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HomeFormEmp homeForm = new HomeFormEmp();
            this.Hide();
            homeForm.ShowDialog();
            this.Show();
        }
    }
}