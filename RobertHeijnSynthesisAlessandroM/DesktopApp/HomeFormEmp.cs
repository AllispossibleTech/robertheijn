﻿using DataAccessLayer;
using DomainLayer;
using DomainLayer.Advertisements;
using DomainLayer.Interfaces;
using DomainLayer.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Linq.Expressions;

namespace DesktopApp
{
    public partial class HomeFormEmp : Form
    {
        Stock stock;
        AdvertisementsManager advertisementsManager;
        UserManager userManager;
        public HomeFormEmp()
        {
            InitializeComponent();
            stock = new Stock(new ProductRepository(), new AdvertisementRepository());
            advertisementsManager = new AdvertisementsManager(new AdvertisementRepository());
            userManager = new UserManager(new UserRepository());
            this.Refresh();
        }

        public void Refresh()
        {
            dgv_products.Rows.Clear();
            dgv_advertisements.Rows.Clear();
            dgv_users.Rows.Clear();

            try
            {
                foreach (Product product in this.stock.GetProducts())
                {
                    dgv_products.Rows.Add(product.Id, product.Name, product.Category, product.SubCategory, product.Price, product.Unit, product.Weight);
                }

                foreach (IProductDiscountAdvertisement a in this.advertisementsManager.GetAdvertisements())
                {
                    if (a is AD_SecondHalfPrice)
                    {
                        dgv_advertisements.Rows.Add(
                            ((Advertisement)a).Product.Id,
                            ((Advertisement)a).StartDate,
                            ((Advertisement)a).EndDate,
                            "SECOND HALF PRICE",
                            "N/A",
                            "N/A",
                            "N/A"
                        );
                    }
                    else if (a is AD_TwoForOne)
                    {
                        dgv_advertisements.Rows.Add(
                            ((Advertisement)a).Product.Id,
                            ((Advertisement)a).StartDate,
                            ((Advertisement)a).EndDate,
                            "TWO FOR ONE",
                            "N/A",
                            "N/A",
                            "N/A"
                        );
                    }
                    else if (a is AD_TwoForPrice)
                    {
                        dgv_advertisements.Rows.Add(
                            ((AD_TwoForPrice)a).Product.Id,
                            ((AD_TwoForPrice)a).StartDate,
                            ((AD_TwoForPrice)a).EndDate,
                            "TWO FOR PRICE",
                            ((AD_TwoForPrice)a).Price,
                            "N/A",
                            "N/A"
                        );
                    }
                    else if (a is AD_PriceForWeight)
                    {
                        dgv_advertisements.Rows.Add(
                            ((AD_PriceForWeight)a).Product.Id,
                            ((AD_PriceForWeight)a).StartDate,
                            ((AD_PriceForWeight)a).EndDate,
                            "TWO FOR PRICE",
                            ((AD_PriceForWeight)a).Price,
                            ((AD_PriceForWeight)a).Unit,
                            ((AD_PriceForWeight)a).Weight
                        );
                    }
                }
            }
            catch(Exception ex)
            {
                // label_errorEx.Text = ex.Message;
            }

            try
            {
                foreach(User u in this.userManager.GetUsers())
                {
                    if (u is Customer)
                    {
                        dgv_users.Rows.Add(
                            u.Id,
                            u.FirstName,
                            u.Surname,
                            u.PhoneNumber,
                            u.Email,
                            "Customer"
                        );
                    }
                    else if (u is Employee)
                    {
                        dgv_users.Rows.Add(
                            u.Id,
                            u.FirstName,
                            u.Surname,
                            u.PhoneNumber,
                            u.Email,
                            "Employee"
                        );
                    }

                }
            }
            catch(Exception ex)
            {
                // label_errorEx.Text = ex.Message;
            }
        }

        private void btn_createProduct_Click(object sender, EventArgs e)
        {
            ProductCreateForm productCreateForm = new ProductCreateForm(this.stock);
            productCreateForm.ShowDialog();
            this.Refresh();
        }

        private void btn_updateProduct_Click(object sender, EventArgs e)
        {
            if (dgv_products.SelectedRows.Count == 0)
            {
                MessageBox.Show("Error: You must select a product.");
                return;
            }

        }

        private void btn_deleteProduct_Click(object sender, EventArgs e)
        {
            if (dgv_products.SelectedRows.Count == 0)
            {
                MessageBox.Show("Error: You must select a product.");
                return;
            }
            int id = (int)dgv_products.SelectedRows[0].Cells[0].Value;
            this.stock.Remove(id);
            this.Refresh();
        }

        private void btn_createAd_Click(object sender, EventArgs e)
        {

            if (dgv_products.SelectedRows.Count == 0)
            {
                MessageBox.Show("Error: You must select a product.");
                return;
            }

            int id = (int)dgv_products.SelectedRows[0].Cells[0].Value;
            Product advertisedProduct = stock.GetProduct(id);

            if (cbx_discountType.Text == "TWO_FOR_ONE")
            {
                advertisementsManager.Add(new AD_TwoForOne(advertisedProduct, dtp_startDate.Value, dtp_endDate.Value));
            }
            else if (cbx_discountType.Text == "SECOND_HALF_PRICE")
            {
                advertisementsManager.Add(new AD_SecondHalfPrice(advertisedProduct, dtp_startDate.Value, dtp_endDate.Value));
            }
            else if (cbx_discountType.Text == "PRICE_FOR_WEIGHT")
            {
                advertisementsManager.Add(new AD_PriceForWeight(advertisedProduct, dtp_startDate.Value, dtp_endDate.Value, Convert.ToDouble(num_price.Value), Convert.ToDouble(num_weight.Value), (Unit)cbx_unit.SelectedIndex));
            }
            else if (cbx_discountType.Text == "TWO_FOR_PRICE")
            {
                advertisementsManager.Add(new AD_TwoForPrice(advertisedProduct, dtp_startDate.Value, dtp_endDate.Value, Convert.ToDouble(num_price.Value)));
            }

            this.Refresh();
        }

        private void btn_deleteAd_Click(object sender, EventArgs e)
        {
            if (dgv_advertisements.SelectedRows.Count == 0)
            {
                MessageBox.Show("Error: You must select an advertisement.");
                return;
            }

            int id = (int)dgv_advertisements.SelectedRows[0].Cells[0].Value;
            this.advertisementsManager.Remove(id);
            this.Refresh();
        }

        private void btn_createUser_Click(object sender, EventArgs e)
        {
            if (cbx_userType.Text == "CUSTOMER")
            {
                try
                {
                    this.userManager.Add(new Customer(
                        txt_userEmail.Text,
                        PasswordHandler.HashPassword(txt_userPassword.Text),
                        txt_userFirstName.Text,
                        txt_userSurname.Text,
                        dtp_userBirthdate.Value,
                        new Address(txt_userStreet.Text, Convert.ToInt32(num_userHouseNumber.Value), txt_userCity.Text, txt_userPostalCode.Text, txt_userCountry.Text),
                        txt_userPhoneNr.Text
                    ));
                }
                catch(Exception ex)
                {
                    // string error = ex.Message
                }
                finally
                {
                    this.Refresh();
                }

            }
            else if (cbx_userType.Text == "EMPLOYEE")
            {
                try
                {
                    this.userManager.Add(new Employee(
                        txt_userEmail.Text,
                        PasswordHandler.HashPassword(txt_userPassword.Text),
                        txt_userFirstName.Text,
                        txt_userSurname.Text,
                        dtp_userBirthdate.Value,
                        new Address(txt_userStreet.Text, Convert.ToInt32(num_userHouseNumber.Value), txt_userCity.Text, txt_userPostalCode.Text, txt_userCountry.Text),
                        txt_userPhoneNr.Text,
                        dtp_employeeContractStart.Value,
                        txt_employeePosition.Text
                    ));
                }
                catch (Exception ex)
                {
                    // string error = ex.Message
                }
                finally
                {
                    this.Refresh();
                }
            }
        }

        private void cbx_userType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbx_userType.Text == "EMPLOYEE")
            {
                dtp_employeeContractStart.Enabled = true;
                txt_employeePosition.Enabled = true;
            }
            else
            {
                dtp_employeeContractStart.Enabled = false;
                txt_employeePosition.Enabled = false;
            }
        }
    }
}
