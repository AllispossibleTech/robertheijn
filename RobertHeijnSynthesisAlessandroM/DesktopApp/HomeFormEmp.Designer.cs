﻿namespace DesktopApp
{
    partial class HomeFormEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbx_Products = new System.Windows.Forms.GroupBox();
            this.btn_deleteProduct = new System.Windows.Forms.Button();
            this.btn_updateProduct = new System.Windows.Forms.Button();
            this.btn_createProduct = new System.Windows.Forms.Button();
            this.dgv_products = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Categpry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gbx_advertisements = new System.Windows.Forms.GroupBox();
            this.num_weight = new System.Windows.Forms.NumericUpDown();
            this.cbx_unit = new System.Windows.Forms.ComboBox();
            this.num_price = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbx_discountType = new System.Windows.Forms.ComboBox();
            this.dtp_endDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_startDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_deleteAd = new System.Windows.Forms.Button();
            this.btn_updateAd = new System.Windows.Forms.Button();
            this.btn_createAd = new System.Windows.Forms.Button();
            this.dgv_advertisements = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscountType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.dtp_employeeContractStart = new System.Windows.Forms.DateTimePicker();
            this.txt_employeePosition = new System.Windows.Forms.TextBox();
            this.cbx_userType = new System.Windows.Forms.ComboBox();
            this.btn_createUser = new System.Windows.Forms.Button();
            this.txt_userPhoneNr = new System.Windows.Forms.TextBox();
            this.txt_userCountry = new System.Windows.Forms.TextBox();
            this.txt_userPostalCode = new System.Windows.Forms.TextBox();
            this.txt_userCity = new System.Windows.Forms.TextBox();
            this.num_userHouseNumber = new System.Windows.Forms.NumericUpDown();
            this.txt_userStreet = new System.Windows.Forms.TextBox();
            this.dtp_userBirthdate = new System.Windows.Forms.DateTimePicker();
            this.txt_userSurname = new System.Windows.Forms.TextBox();
            this.txt_userFirstName = new System.Windows.Forms.TextBox();
            this.txt_userPassword = new System.Windows.Forms.TextBox();
            this.txt_userEmail = new System.Windows.Forms.TextBox();
            this.dgv_users = new System.Windows.Forms.DataGridView();
            this.dgv_users_Column_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_users_Column_FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_users_Column_Surname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_users_Column_PhoneNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_users_Column_Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.gbx_Products.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_products)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gbx_advertisements.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_advertisements)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_userHouseNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_users)).BeginInit();
            this.SuspendLayout();
            // 
            // gbx_Products
            // 
            this.gbx_Products.Controls.Add(this.btn_deleteProduct);
            this.gbx_Products.Controls.Add(this.btn_updateProduct);
            this.gbx_Products.Controls.Add(this.btn_createProduct);
            this.gbx_Products.Controls.Add(this.dgv_products);
            this.gbx_Products.Location = new System.Drawing.Point(40, 46);
            this.gbx_Products.Name = "gbx_Products";
            this.gbx_Products.Size = new System.Drawing.Size(1511, 452);
            this.gbx_Products.TabIndex = 0;
            this.gbx_Products.TabStop = false;
            this.gbx_Products.Text = "Products";
            // 
            // btn_deleteProduct
            // 
            this.btn_deleteProduct.Location = new System.Drawing.Point(1146, 240);
            this.btn_deleteProduct.Name = "btn_deleteProduct";
            this.btn_deleteProduct.Size = new System.Drawing.Size(193, 102);
            this.btn_deleteProduct.TabIndex = 3;
            this.btn_deleteProduct.Text = "Delete";
            this.btn_deleteProduct.UseVisualStyleBackColor = true;
            this.btn_deleteProduct.Click += new System.EventHandler(this.btn_deleteProduct_Click);
            // 
            // btn_updateProduct
            // 
            this.btn_updateProduct.Location = new System.Drawing.Point(1242, 132);
            this.btn_updateProduct.Name = "btn_updateProduct";
            this.btn_updateProduct.Size = new System.Drawing.Size(193, 102);
            this.btn_updateProduct.TabIndex = 2;
            this.btn_updateProduct.Text = "Update";
            this.btn_updateProduct.UseVisualStyleBackColor = true;
            this.btn_updateProduct.Click += new System.EventHandler(this.btn_updateProduct_Click);
            // 
            // btn_createProduct
            // 
            this.btn_createProduct.Location = new System.Drawing.Point(1043, 132);
            this.btn_createProduct.Name = "btn_createProduct";
            this.btn_createProduct.Size = new System.Drawing.Size(193, 102);
            this.btn_createProduct.TabIndex = 1;
            this.btn_createProduct.Text = "Create";
            this.btn_createProduct.UseVisualStyleBackColor = true;
            this.btn_createProduct.Click += new System.EventHandler(this.btn_createProduct_Click);
            // 
            // dgv_products
            // 
            this.dgv_products.AllowUserToAddRows = false;
            this.dgv_products.AllowUserToDeleteRows = false;
            this.dgv_products.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_products.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_products.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.ProductName,
            this.Categpry,
            this.SubCategory,
            this.Price,
            this.Unit,
            this.Weight});
            this.dgv_products.Location = new System.Drawing.Point(19, 52);
            this.dgv_products.Name = "dgv_products";
            this.dgv_products.ReadOnly = true;
            this.dgv_products.RowHeadersVisible = false;
            this.dgv_products.RowHeadersWidth = 82;
            this.dgv_products.RowTemplate.Height = 41;
            this.dgv_products.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_products.Size = new System.Drawing.Size(914, 374);
            this.dgv_products.TabIndex = 0;
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.MinimumWidth = 10;
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 79;
            // 
            // ProductName
            // 
            this.ProductName.HeaderText = "Name";
            this.ProductName.MinimumWidth = 10;
            this.ProductName.Name = "ProductName";
            this.ProductName.ReadOnly = true;
            this.ProductName.Width = 123;
            // 
            // Categpry
            // 
            this.Categpry.HeaderText = "Category";
            this.Categpry.MinimumWidth = 10;
            this.Categpry.Name = "Categpry";
            this.Categpry.ReadOnly = true;
            this.Categpry.Width = 155;
            // 
            // SubCategory
            // 
            this.SubCategory.HeaderText = "Sub-Category";
            this.SubCategory.MinimumWidth = 10;
            this.SubCategory.Name = "SubCategory";
            this.SubCategory.ReadOnly = true;
            this.SubCategory.Width = 206;
            // 
            // Price
            // 
            this.Price.HeaderText = "Price";
            this.Price.MinimumWidth = 10;
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 110;
            // 
            // Unit
            // 
            this.Unit.HeaderText = "Unit";
            this.Unit.MinimumWidth = 10;
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            this.Unit.Width = 103;
            // 
            // Weight
            // 
            this.Weight.HeaderText = "Weight";
            this.Weight.MinimumWidth = 10;
            this.Weight.Name = "Weight";
            this.Weight.ReadOnly = true;
            this.Weight.Width = 135;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Location = new System.Drawing.Point(41, 45);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1604, 1280);
            this.tabControl.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gbx_advertisements);
            this.tabPage1.Controls.Add(this.gbx_Products);
            this.tabPage1.Location = new System.Drawing.Point(8, 46);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1588, 1226);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Products & Ads";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gbx_advertisements
            // 
            this.gbx_advertisements.Controls.Add(this.num_weight);
            this.gbx_advertisements.Controls.Add(this.cbx_unit);
            this.gbx_advertisements.Controls.Add(this.num_price);
            this.gbx_advertisements.Controls.Add(this.label6);
            this.gbx_advertisements.Controls.Add(this.label5);
            this.gbx_advertisements.Controls.Add(this.label4);
            this.gbx_advertisements.Controls.Add(this.label3);
            this.gbx_advertisements.Controls.Add(this.cbx_discountType);
            this.gbx_advertisements.Controls.Add(this.dtp_endDate);
            this.gbx_advertisements.Controls.Add(this.dtp_startDate);
            this.gbx_advertisements.Controls.Add(this.label2);
            this.gbx_advertisements.Controls.Add(this.label1);
            this.gbx_advertisements.Controls.Add(this.btn_deleteAd);
            this.gbx_advertisements.Controls.Add(this.btn_updateAd);
            this.gbx_advertisements.Controls.Add(this.btn_createAd);
            this.gbx_advertisements.Controls.Add(this.dgv_advertisements);
            this.gbx_advertisements.Location = new System.Drawing.Point(40, 519);
            this.gbx_advertisements.Name = "gbx_advertisements";
            this.gbx_advertisements.Size = new System.Drawing.Size(1511, 688);
            this.gbx_advertisements.TabIndex = 1;
            this.gbx_advertisements.TabStop = false;
            this.gbx_advertisements.Text = "Advertisements";
            // 
            // num_weight
            // 
            this.num_weight.Location = new System.Drawing.Point(1043, 510);
            this.num_weight.Name = "num_weight";
            this.num_weight.Size = new System.Drawing.Size(204, 39);
            this.num_weight.TabIndex = 18;
            // 
            // cbx_unit
            // 
            this.cbx_unit.FormattingEnabled = true;
            this.cbx_unit.Items.AddRange(new object[] {
            "NO_UNIT",
            "KILO",
            "GRAM"});
            this.cbx_unit.Location = new System.Drawing.Point(1274, 415);
            this.cbx_unit.Name = "cbx_unit";
            this.cbx_unit.Size = new System.Drawing.Size(177, 40);
            this.cbx_unit.TabIndex = 17;
            // 
            // num_price
            // 
            this.num_price.Location = new System.Drawing.Point(1043, 416);
            this.num_price.Name = "num_price";
            this.num_price.Size = new System.Drawing.Size(204, 39);
            this.num_price.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1043, 469);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(199, 32);
            this.label6.TabIndex = 15;
            this.label6.Text = "6. Specify Weight";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1274, 369);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 32);
            this.label5.TabIndex = 14;
            this.label5.Text = "5. Specify Unit";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1043, 369);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 32);
            this.label4.TabIndex = 13;
            this.label4.Text = "4. Specify Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1043, 268);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(304, 32);
            this.label3.TabIndex = 12;
            this.label3.Text = "3. Specify Type of Discount";
            // 
            // cbx_discountType
            // 
            this.cbx_discountType.FormattingEnabled = true;
            this.cbx_discountType.Items.AddRange(new object[] {
            "TWO_FOR_ONE",
            "TWO_FOR_PRICE",
            "PRICE_FOR_WEIGHT",
            "SECOND_HALF_PRICE"});
            this.cbx_discountType.Location = new System.Drawing.Point(1043, 313);
            this.cbx_discountType.Name = "cbx_discountType";
            this.cbx_discountType.Size = new System.Drawing.Size(400, 40);
            this.cbx_discountType.TabIndex = 11;
            // 
            // dtp_endDate
            // 
            this.dtp_endDate.Location = new System.Drawing.Point(1043, 211);
            this.dtp_endDate.Name = "dtp_endDate";
            this.dtp_endDate.Size = new System.Drawing.Size(400, 39);
            this.dtp_endDate.TabIndex = 10;
            // 
            // dtp_startDate
            // 
            this.dtp_startDate.Location = new System.Drawing.Point(1043, 150);
            this.dtp_startDate.Name = "dtp_startDate";
            this.dtp_startDate.Size = new System.Drawing.Size(400, 39);
            this.dtp_startDate.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1043, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(322, 32);
            this.label2.TabIndex = 8;
            this.label2.Text = "2. Specify Start and End Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1043, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(359, 32);
            this.label1.TabIndex = 7;
            this.label1.Text = "1. Select Product from above list";
            // 
            // btn_deleteAd
            // 
            this.btn_deleteAd.Location = new System.Drawing.Point(1309, 565);
            this.btn_deleteAd.Name = "btn_deleteAd";
            this.btn_deleteAd.Size = new System.Drawing.Size(134, 102);
            this.btn_deleteAd.TabIndex = 6;
            this.btn_deleteAd.Text = "Delete";
            this.btn_deleteAd.UseVisualStyleBackColor = true;
            this.btn_deleteAd.Click += new System.EventHandler(this.btn_deleteAd_Click);
            // 
            // btn_updateAd
            // 
            this.btn_updateAd.Location = new System.Drawing.Point(1175, 565);
            this.btn_updateAd.Name = "btn_updateAd";
            this.btn_updateAd.Size = new System.Drawing.Size(128, 102);
            this.btn_updateAd.TabIndex = 5;
            this.btn_updateAd.Text = "Update";
            this.btn_updateAd.UseVisualStyleBackColor = true;
            // 
            // btn_createAd
            // 
            this.btn_createAd.Location = new System.Drawing.Point(1040, 565);
            this.btn_createAd.Name = "btn_createAd";
            this.btn_createAd.Size = new System.Drawing.Size(129, 102);
            this.btn_createAd.TabIndex = 4;
            this.btn_createAd.Text = "Create";
            this.btn_createAd.UseVisualStyleBackColor = true;
            this.btn_createAd.Click += new System.EventHandler(this.btn_createAd_Click);
            // 
            // dgv_advertisements
            // 
            this.dgv_advertisements.AllowUserToAddRows = false;
            this.dgv_advertisements.AllowUserToDeleteRows = false;
            this.dgv_advertisements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_advertisements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_advertisements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.StartDate,
            this.EndDate,
            this.DiscountType,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.dgv_advertisements.Location = new System.Drawing.Point(19, 52);
            this.dgv_advertisements.Name = "dgv_advertisements";
            this.dgv_advertisements.ReadOnly = true;
            this.dgv_advertisements.RowHeadersVisible = false;
            this.dgv_advertisements.RowHeadersWidth = 82;
            this.dgv_advertisements.RowTemplate.Height = 41;
            this.dgv_advertisements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_advertisements.Size = new System.Drawing.Size(962, 615);
            this.dgv_advertisements.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 10;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 79;
            // 
            // StartDate
            // 
            this.StartDate.HeaderText = "Start Date";
            this.StartDate.MinimumWidth = 10;
            this.StartDate.Name = "StartDate";
            this.StartDate.ReadOnly = true;
            this.StartDate.Width = 164;
            // 
            // EndDate
            // 
            this.EndDate.HeaderText = "End Date";
            this.EndDate.MinimumWidth = 10;
            this.EndDate.Name = "EndDate";
            this.EndDate.ReadOnly = true;
            this.EndDate.Width = 156;
            // 
            // DiscountType
            // 
            this.DiscountType.HeaderText = "Discount Type";
            this.DiscountType.MinimumWidth = 10;
            this.DiscountType.Name = "DiscountType";
            this.DiscountType.ReadOnly = true;
            this.DiscountType.Width = 211;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Price";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 10;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 110;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Unit";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 10;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 103;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Weight";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 10;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 135;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label222);
            this.tabPage2.Controls.Add(this.dtp_employeeContractStart);
            this.tabPage2.Controls.Add(this.txt_employeePosition);
            this.tabPage2.Controls.Add(this.cbx_userType);
            this.tabPage2.Controls.Add(this.btn_createUser);
            this.tabPage2.Controls.Add(this.txt_userPhoneNr);
            this.tabPage2.Controls.Add(this.txt_userCountry);
            this.tabPage2.Controls.Add(this.txt_userPostalCode);
            this.tabPage2.Controls.Add(this.txt_userCity);
            this.tabPage2.Controls.Add(this.num_userHouseNumber);
            this.tabPage2.Controls.Add(this.txt_userStreet);
            this.tabPage2.Controls.Add(this.dtp_userBirthdate);
            this.tabPage2.Controls.Add(this.txt_userSurname);
            this.tabPage2.Controls.Add(this.txt_userFirstName);
            this.tabPage2.Controls.Add(this.txt_userPassword);
            this.tabPage2.Controls.Add(this.txt_userEmail);
            this.tabPage2.Controls.Add(this.dgv_users);
            this.tabPage2.Location = new System.Drawing.Point(8, 46);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1588, 1226);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Users";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(1187, 1089);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(328, 32);
            this.label19.TabIndex = 30;
            this.label19.Text = "Employee Contract Start Date";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(867, 1089);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(210, 32);
            this.label18.TabIndex = 29;
            this.label18.Text = "Employee Position";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(1115, 1004);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(119, 32);
            this.label17.TabIndex = 28;
            this.label17.Text = "User Type";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(1115, 919);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(177, 32);
            this.label16.TabIndex = 27;
            this.label16.Text = "Phone Number";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1115, 832);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(99, 32);
            this.label15.TabIndex = 26;
            this.label15.Text = "Country";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1115, 654);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 32);
            this.label14.TabIndex = 25;
            this.label14.Text = "City";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1113, 741);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(139, 32);
            this.label13.TabIndex = 24;
            this.label13.Text = "Postal Code";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1113, 577);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(177, 32);
            this.label12.TabIndex = 23;
            this.label12.Text = "House Number";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1113, 495);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(167, 32);
            this.label11.TabIndex = 22;
            this.label11.Text = "Address Street";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1115, 383);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 32);
            this.label10.TabIndex = 21;
            this.label10.Text = "Birthdate";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1113, 294);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 32);
            this.label9.TabIndex = 20;
            this.label9.Text = "Surname";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1113, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 32);
            this.label8.TabIndex = 19;
            this.label8.Text = "First Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1113, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 32);
            this.label7.TabIndex = 18;
            this.label7.Text = "Password";
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Location = new System.Drawing.Point(1113, 28);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(71, 32);
            this.label222.TabIndex = 17;
            this.label222.Text = "Email";
            // 
            // dtp_employeeContractStart
            // 
            this.dtp_employeeContractStart.Enabled = false;
            this.dtp_employeeContractStart.Location = new System.Drawing.Point(1187, 1124);
            this.dtp_employeeContractStart.Name = "dtp_employeeContractStart";
            this.dtp_employeeContractStart.Size = new System.Drawing.Size(372, 39);
            this.dtp_employeeContractStart.TabIndex = 16;
            // 
            // txt_employeePosition
            // 
            this.txt_employeePosition.Enabled = false;
            this.txt_employeePosition.Location = new System.Drawing.Point(867, 1124);
            this.txt_employeePosition.Name = "txt_employeePosition";
            this.txt_employeePosition.Size = new System.Drawing.Size(314, 39);
            this.txt_employeePosition.TabIndex = 15;
            // 
            // cbx_userType
            // 
            this.cbx_userType.FormattingEnabled = true;
            this.cbx_userType.Items.AddRange(new object[] {
            "EMPLOYEE",
            "CUSTOMER"});
            this.cbx_userType.Location = new System.Drawing.Point(1113, 1039);
            this.cbx_userType.Name = "cbx_userType";
            this.cbx_userType.Size = new System.Drawing.Size(376, 40);
            this.cbx_userType.TabIndex = 14;
            this.cbx_userType.SelectedIndexChanged += new System.EventHandler(this.cbx_userType_SelectedIndexChanged);
            // 
            // btn_createUser
            // 
            this.btn_createUser.Location = new System.Drawing.Point(1164, 1178);
            this.btn_createUser.Name = "btn_createUser";
            this.btn_createUser.Size = new System.Drawing.Size(282, 42);
            this.btn_createUser.TabIndex = 13;
            this.btn_createUser.Text = "Create";
            this.btn_createUser.UseVisualStyleBackColor = true;
            this.btn_createUser.Click += new System.EventHandler(this.btn_createUser_Click);
            // 
            // txt_userPhoneNr
            // 
            this.txt_userPhoneNr.Location = new System.Drawing.Point(1113, 954);
            this.txt_userPhoneNr.Name = "txt_userPhoneNr";
            this.txt_userPhoneNr.Size = new System.Drawing.Size(376, 39);
            this.txt_userPhoneNr.TabIndex = 12;
            // 
            // txt_userCountry
            // 
            this.txt_userCountry.Location = new System.Drawing.Point(1113, 867);
            this.txt_userCountry.Name = "txt_userCountry";
            this.txt_userCountry.Size = new System.Drawing.Size(376, 39);
            this.txt_userCountry.TabIndex = 11;
            // 
            // txt_userPostalCode
            // 
            this.txt_userPostalCode.Location = new System.Drawing.Point(1113, 776);
            this.txt_userPostalCode.Name = "txt_userPostalCode";
            this.txt_userPostalCode.Size = new System.Drawing.Size(376, 39);
            this.txt_userPostalCode.TabIndex = 10;
            // 
            // txt_userCity
            // 
            this.txt_userCity.Location = new System.Drawing.Point(1113, 692);
            this.txt_userCity.Name = "txt_userCity";
            this.txt_userCity.Size = new System.Drawing.Size(376, 39);
            this.txt_userCity.TabIndex = 9;
            // 
            // num_userHouseNumber
            // 
            this.num_userHouseNumber.Location = new System.Drawing.Point(1249, 612);
            this.num_userHouseNumber.Name = "num_userHouseNumber";
            this.num_userHouseNumber.Size = new System.Drawing.Size(240, 39);
            this.num_userHouseNumber.TabIndex = 8;
            // 
            // txt_userStreet
            // 
            this.txt_userStreet.Location = new System.Drawing.Point(1113, 530);
            this.txt_userStreet.Name = "txt_userStreet";
            this.txt_userStreet.Size = new System.Drawing.Size(376, 39);
            this.txt_userStreet.TabIndex = 7;
            // 
            // dtp_userBirthdate
            // 
            this.dtp_userBirthdate.Location = new System.Drawing.Point(1113, 418);
            this.dtp_userBirthdate.Name = "dtp_userBirthdate";
            this.dtp_userBirthdate.Size = new System.Drawing.Size(376, 39);
            this.dtp_userBirthdate.TabIndex = 6;
            // 
            // txt_userSurname
            // 
            this.txt_userSurname.Location = new System.Drawing.Point(1113, 329);
            this.txt_userSurname.Name = "txt_userSurname";
            this.txt_userSurname.Size = new System.Drawing.Size(376, 39);
            this.txt_userSurname.TabIndex = 5;
            // 
            // txt_userFirstName
            // 
            this.txt_userFirstName.Location = new System.Drawing.Point(1113, 240);
            this.txt_userFirstName.Name = "txt_userFirstName";
            this.txt_userFirstName.Size = new System.Drawing.Size(376, 39);
            this.txt_userFirstName.TabIndex = 4;
            // 
            // txt_userPassword
            // 
            this.txt_userPassword.Location = new System.Drawing.Point(1113, 160);
            this.txt_userPassword.Name = "txt_userPassword";
            this.txt_userPassword.Size = new System.Drawing.Size(376, 39);
            this.txt_userPassword.TabIndex = 3;
            // 
            // txt_userEmail
            // 
            this.txt_userEmail.Location = new System.Drawing.Point(1113, 73);
            this.txt_userEmail.Name = "txt_userEmail";
            this.txt_userEmail.Size = new System.Drawing.Size(376, 39);
            this.txt_userEmail.TabIndex = 2;
            // 
            // dgv_users
            // 
            this.dgv_users.AllowUserToAddRows = false;
            this.dgv_users.AllowUserToDeleteRows = false;
            this.dgv_users.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_users.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_users_Column_Id,
            this.dgv_users_Column_FirstName,
            this.dgv_users_Column_Surname,
            this.dgv_users_Column_PhoneNr,
            this.dgv_users_Column_Email,
            this.UserType});
            this.dgv_users.Location = new System.Drawing.Point(46, 63);
            this.dgv_users.Name = "dgv_users";
            this.dgv_users.ReadOnly = true;
            this.dgv_users.RowHeadersVisible = false;
            this.dgv_users.RowHeadersWidth = 82;
            this.dgv_users.RowTemplate.Height = 41;
            this.dgv_users.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_users.Size = new System.Drawing.Size(1031, 977);
            this.dgv_users.TabIndex = 1;
            // 
            // dgv_users_Column_Id
            // 
            this.dgv_users_Column_Id.HeaderText = "Id";
            this.dgv_users_Column_Id.MinimumWidth = 10;
            this.dgv_users_Column_Id.Name = "dgv_users_Column_Id";
            this.dgv_users_Column_Id.ReadOnly = true;
            this.dgv_users_Column_Id.Width = 79;
            // 
            // dgv_users_Column_FirstName
            // 
            this.dgv_users_Column_FirstName.HeaderText = "FirstName";
            this.dgv_users_Column_FirstName.MinimumWidth = 10;
            this.dgv_users_Column_FirstName.Name = "dgv_users_Column_FirstName";
            this.dgv_users_Column_FirstName.ReadOnly = true;
            this.dgv_users_Column_FirstName.Width = 167;
            // 
            // dgv_users_Column_Surname
            // 
            this.dgv_users_Column_Surname.HeaderText = "Surname";
            this.dgv_users_Column_Surname.MinimumWidth = 10;
            this.dgv_users_Column_Surname.Name = "dgv_users_Column_Surname";
            this.dgv_users_Column_Surname.ReadOnly = true;
            this.dgv_users_Column_Surname.Width = 154;
            // 
            // dgv_users_Column_PhoneNr
            // 
            this.dgv_users_Column_PhoneNr.HeaderText = "Phone Nr";
            this.dgv_users_Column_PhoneNr.MinimumWidth = 10;
            this.dgv_users_Column_PhoneNr.Name = "dgv_users_Column_PhoneNr";
            this.dgv_users_Column_PhoneNr.ReadOnly = true;
            this.dgv_users_Column_PhoneNr.Width = 160;
            // 
            // dgv_users_Column_Email
            // 
            this.dgv_users_Column_Email.HeaderText = "Email";
            this.dgv_users_Column_Email.MinimumWidth = 10;
            this.dgv_users_Column_Email.Name = "dgv_users_Column_Email";
            this.dgv_users_Column_Email.ReadOnly = true;
            this.dgv_users_Column_Email.Width = 116;
            // 
            // UserType
            // 
            this.UserType.HeaderText = "User Type";
            this.UserType.MinimumWidth = 10;
            this.UserType.Name = "UserType";
            this.UserType.ReadOnly = true;
            this.UserType.Width = 164;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(55, 1089);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(282, 42);
            this.button1.TabIndex = 31;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(362, 1089);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(282, 42);
            this.button2.TabIndex = 32;
            this.button2.Text = "Update";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // HomeFormEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1678, 1346);
            this.Controls.Add(this.tabControl);
            this.Name = "HomeFormEmp";
            this.Text = "HomeForm";
            this.gbx_Products.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_products)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.gbx_advertisements.ResumeLayout(false);
            this.gbx_advertisements.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_advertisements)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_userHouseNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_users)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox gbx_Products;
        private DataGridView dgv_products;
        private DataGridViewTextBoxColumn Id;
        private DataGridViewTextBoxColumn ProductName;
        private DataGridViewTextBoxColumn Categpry;
        private DataGridViewTextBoxColumn SubCategory;
        private DataGridViewTextBoxColumn Price;
        private DataGridViewTextBoxColumn Unit;
        private DataGridViewTextBoxColumn Weight;
        private TabControl tabControl;
        private TabPage tabPage1;
        private GroupBox gbx_advertisements;
        private DataGridView dgv_advertisements;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn StartDate;
        private DataGridViewTextBoxColumn EndDate;
        private DataGridViewTextBoxColumn DiscountType;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private TabPage tabPage2;
        private Button btn_deleteProduct;
        private Button btn_updateProduct;
        private Button btn_createProduct;
        private Button btn_deleteAd;
        private Button btn_updateAd;
        private Button btn_createAd;
        private DateTimePicker dtp_endDate;
        private DateTimePicker dtp_startDate;
        private Label label2;
        private Label label1;
        private Label label3;
        private ComboBox cbx_discountType;
        private NumericUpDown num_weight;
        private ComboBox cbx_unit;
        private NumericUpDown num_price;
        private Label label6;
        private Label label5;
        private Label label4;
        private DataGridView dgv_users;
        private TextBox txt_userEmail;
        private TextBox txt_userPassword;
        private TextBox txt_userSurname;
        private TextBox txt_userFirstName;
        private DateTimePicker dtp_userBirthdate;
        private TextBox txt_userStreet;
        private NumericUpDown num_userHouseNumber;
        private TextBox txt_userPostalCode;
        private TextBox txt_userCity;
        private TextBox txt_userCountry;
        private Button btn_createUser;
        private TextBox txt_userPhoneNr;
        private TextBox txt_employeePosition;
        private ComboBox cbx_userType;
        private DataGridViewTextBoxColumn dgv_users_Column_Id;
        private DataGridViewTextBoxColumn dgv_users_Column_FirstName;
        private DataGridViewTextBoxColumn dgv_users_Column_Surname;
        private DataGridViewTextBoxColumn dgv_users_Column_PhoneNr;
        private DataGridViewTextBoxColumn dgv_users_Column_Email;
        private DataGridViewTextBoxColumn UserType;
        private DateTimePicker dtp_employeeContractStart;
        private Label label13;
        private Label label12;
        private Label label11;
        private Label label10;
        private Label label9;
        private Label label8;
        private Label label7;
        private Label label222;
        private Label label19;
        private Label label18;
        private Label label17;
        private Label label16;
        private Label label15;
        private Label label14;
        private Button button2;
        private Button button1;
    }
}