﻿namespace DesktopApp
{
    partial class ProductCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_name = new System.Windows.Forms.TextBox();
            this.txt_category = new System.Windows.Forms.TextBox();
            this.txt_subCategory = new System.Windows.Forms.TextBox();
            this.num_price = new System.Windows.Forms.NumericUpDown();
            this.cbx_unit = new System.Windows.Forms.ComboBox();
            this.num_weight = new System.Windows.Forms.NumericUpDown();
            this.lbl_name = new System.Windows.Forms.Label();
            this.lbl_category = new System.Windows.Forms.Label();
            this.lbl_subCategory = new System.Windows.Forms.Label();
            this.lbl_price = new System.Windows.Forms.Label();
            this.lbl_unit = new System.Windows.Forms.Label();
            this.lbl_weight = new System.Windows.Forms.Label();
            this.lbl_product = new System.Windows.Forms.Label();
            this.btn_confirm = new System.Windows.Forms.Button();
            this.lbl_exError = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.num_price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_weight)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(197, 165);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(415, 39);
            this.txt_name.TabIndex = 0;
            // 
            // txt_category
            // 
            this.txt_category.Location = new System.Drawing.Point(197, 256);
            this.txt_category.Name = "txt_category";
            this.txt_category.Size = new System.Drawing.Size(415, 39);
            this.txt_category.TabIndex = 1;
            // 
            // txt_subCategory
            // 
            this.txt_subCategory.Location = new System.Drawing.Point(197, 354);
            this.txt_subCategory.Name = "txt_subCategory";
            this.txt_subCategory.Size = new System.Drawing.Size(415, 39);
            this.txt_subCategory.TabIndex = 2;
            // 
            // num_price
            // 
            this.num_price.Location = new System.Drawing.Point(197, 445);
            this.num_price.Name = "num_price";
            this.num_price.Size = new System.Drawing.Size(415, 39);
            this.num_price.TabIndex = 3;
            // 
            // cbx_unit
            // 
            this.cbx_unit.FormattingEnabled = true;
            this.cbx_unit.Items.AddRange(new object[] {
            "NO_UNIT,",
            "KILO,",
            "GRAM"});
            this.cbx_unit.Location = new System.Drawing.Point(197, 541);
            this.cbx_unit.Name = "cbx_unit";
            this.cbx_unit.Size = new System.Drawing.Size(415, 40);
            this.cbx_unit.TabIndex = 4;
            // 
            // num_weight
            // 
            this.num_weight.Location = new System.Drawing.Point(197, 632);
            this.num_weight.Name = "num_weight";
            this.num_weight.Size = new System.Drawing.Size(415, 39);
            this.num_weight.TabIndex = 5;
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Location = new System.Drawing.Point(81, 172);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(83, 32);
            this.lbl_name.TabIndex = 6;
            this.lbl_name.Text = "Name:";
            // 
            // lbl_category
            // 
            this.lbl_category.AutoSize = true;
            this.lbl_category.Location = new System.Drawing.Point(49, 263);
            this.lbl_category.Name = "lbl_category";
            this.lbl_category.Size = new System.Drawing.Size(115, 32);
            this.lbl_category.TabIndex = 7;
            this.lbl_category.Text = "Category:";
            // 
            // lbl_subCategory
            // 
            this.lbl_subCategory.AutoSize = true;
            this.lbl_subCategory.Location = new System.Drawing.Point(12, 361);
            this.lbl_subCategory.Name = "lbl_subCategory";
            this.lbl_subCategory.Size = new System.Drawing.Size(166, 32);
            this.lbl_subCategory.TabIndex = 8;
            this.lbl_subCategory.Text = "Sub-Category:";
            // 
            // lbl_price
            // 
            this.lbl_price.AutoSize = true;
            this.lbl_price.Location = new System.Drawing.Point(81, 447);
            this.lbl_price.Name = "lbl_price";
            this.lbl_price.Size = new System.Drawing.Size(70, 32);
            this.lbl_price.TabIndex = 9;
            this.lbl_price.Text = "Price:";
            // 
            // lbl_unit
            // 
            this.lbl_unit.AutoSize = true;
            this.lbl_unit.Location = new System.Drawing.Point(81, 544);
            this.lbl_unit.Name = "lbl_unit";
            this.lbl_unit.Size = new System.Drawing.Size(63, 32);
            this.lbl_unit.TabIndex = 10;
            this.lbl_unit.Text = "Unit:";
            // 
            // lbl_weight
            // 
            this.lbl_weight.AutoSize = true;
            this.lbl_weight.Location = new System.Drawing.Point(49, 632);
            this.lbl_weight.Name = "lbl_weight";
            this.lbl_weight.Size = new System.Drawing.Size(95, 32);
            this.lbl_weight.TabIndex = 11;
            this.lbl_weight.Text = "Weight:";
            // 
            // lbl_product
            // 
            this.lbl_product.AutoSize = true;
            this.lbl_product.Font = new System.Drawing.Font("Segoe UI Black", 19.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_product.Location = new System.Drawing.Point(250, 48);
            this.lbl_product.Name = "lbl_product";
            this.lbl_product.Size = new System.Drawing.Size(237, 71);
            this.lbl_product.TabIndex = 12;
            this.lbl_product.Text = "Product";
            // 
            // btn_confirm
            // 
            this.btn_confirm.Location = new System.Drawing.Point(277, 738);
            this.btn_confirm.Name = "btn_confirm";
            this.btn_confirm.Size = new System.Drawing.Size(150, 46);
            this.btn_confirm.TabIndex = 13;
            this.btn_confirm.Text = "Confirm";
            this.btn_confirm.UseVisualStyleBackColor = true;
            this.btn_confirm.Click += new System.EventHandler(this.btn_confirm_Click);
            // 
            // lbl_exError
            // 
            this.lbl_exError.AutoSize = true;
            this.lbl_exError.Location = new System.Drawing.Point(27, 803);
            this.lbl_exError.Name = "lbl_exError";
            this.lbl_exError.Size = new System.Drawing.Size(69, 32);
            this.lbl_exError.TabIndex = 15;
            this.lbl_exError.Text = "Error:";
            this.lbl_exError.Visible = false;
            // 
            // ProductCreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 860);
            this.Controls.Add(this.lbl_exError);
            this.Controls.Add(this.btn_confirm);
            this.Controls.Add(this.lbl_product);
            this.Controls.Add(this.lbl_weight);
            this.Controls.Add(this.lbl_unit);
            this.Controls.Add(this.lbl_price);
            this.Controls.Add(this.lbl_subCategory);
            this.Controls.Add(this.lbl_category);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.num_weight);
            this.Controls.Add(this.cbx_unit);
            this.Controls.Add(this.num_price);
            this.Controls.Add(this.txt_subCategory);
            this.Controls.Add(this.txt_category);
            this.Controls.Add(this.txt_name);
            this.Name = "ProductCreateForm";
            this.Text = "ProductCreateForm";
            ((System.ComponentModel.ISupportInitialize)(this.num_price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_weight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox txt_name;
        private TextBox txt_category;
        private TextBox txt_subCategory;
        private NumericUpDown num_price;
        private ComboBox cbx_unit;
        private NumericUpDown num_weight;
        private Label lbl_name;
        private Label lbl_category;
        private Label lbl_subCategory;
        private Label lbl_price;
        private Label lbl_unit;
        private Label lbl_weight;
        private Label lbl_product;
        private Button btn_confirm;
        private Label lbl_exError;
    }
}