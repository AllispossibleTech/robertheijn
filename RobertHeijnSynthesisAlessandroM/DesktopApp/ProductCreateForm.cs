﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DomainLayer;
using DomainLayer.Enums;

namespace DesktopApp
{
    public partial class ProductCreateForm : Form
    {
        Stock stock;

        public ProductCreateForm(Stock stock)
        {
            InitializeComponent();
            this.stock = stock;
        }

        private void btn_confirm_Click(object sender, EventArgs e)
        {
            try
            {
                stock.Add(new Product(
                    txt_name.Text,
                    txt_category.Text,
                    txt_subCategory.Text,
                    Convert.ToDouble(num_price.Value),
                    (Unit)cbx_unit.SelectedIndex,
                    Convert.ToDouble(num_weight.Value)
                ));
                this.Close();
            }
            catch(Exception ex)
            {
                lbl_exError.Visible = true;
                lbl_exError.Text = ex.Message;
            }
        }
    }
}
