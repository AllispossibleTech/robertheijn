﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace WebApp.Pages.Models
{
    public class Credentials
    {
        [DisplayName("Username")]
        [Required(ErrorMessage = "Please fill in your Email")]
        [EmailAddress]
        public string Username { get; set; }


        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please fill in your Password")]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

    }
}
