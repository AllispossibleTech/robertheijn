﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Pages.Models
{
    public class Customer
    {
        [Required(ErrorMessage = "Please specify your Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please specify your Password")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please specify your Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please specify your Surname")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Please specify your Birthdate")]
        public DateTime Birthdate { get; set; }
        [Required(ErrorMessage = "Please specify your Address Street")]
        public string Street { get; set; }
        [Required(ErrorMessage = "Please specify your Address House Number")]
        public int HouseNumber { get; set; }
        [Required(ErrorMessage = "Please specify your Address City")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please specify your Address Postal Code")]
        public string PostalCode { get; set; }
        [Required(ErrorMessage = "Please specify your Address Country")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Please specify your Phone Number")]
        public string Phone { get; set; }
    }
}
