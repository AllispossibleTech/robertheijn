﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Pages.Models
{
    public class CartItem
    {
        [Required(ErrorMessage = "Please select the amount you want to add to the cart")]
        public int Quantity { get; set; }
    }
}
