using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainLayer;
using DataAccessLayer;

namespace WebApp.Pages
{
    public class productsModel : PageModel
    {
        public List<Product> Products { get; set; }
        public void OnGet()
        {
            Stock stock = new Stock(new ProductRepository(), new AdvertisementRepository());
            this.Products = stock.GetProducts();
        }
    }
}
