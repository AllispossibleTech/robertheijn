﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainLayer;
using DataAccessLayer;

namespace WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        public List<Advertisement> Advertisements { get; private set; }

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            AdvertisementsManager adsManager = new AdvertisementsManager(new AdvertisementRepository());
            this.Advertisements = adsManager.GetActiveAdvertisements();
        }
    }
}