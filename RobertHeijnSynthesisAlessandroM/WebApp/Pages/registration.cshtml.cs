using DomainLayer;
using DataAccessLayer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages
{
    public class registrationModel : PageModel
    {
        [BindProperty]
        public Models.Customer Customer { get; set; }
        public IActionResult OnGet()
        {
            // If user is logged in, he cannot access registration page!

            if(User.Identity.IsAuthenticated)
            {
                return RedirectToPage("index");
            }
            return Page();
        }
        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                // Hashing the Password
                string hashed = PasswordHandler.HashPassword(Customer.Password);

                // Adding the User
                UserManager userManager = new UserManager(new UserRepository());
                userManager.Add(new Customer(Customer.Email, hashed, Customer.Name, Customer.Surname, Customer.Birthdate, new Address(Customer.Street, Customer.HouseNumber, Customer.City, Customer.PostalCode, Customer.Country), Customer.Phone));

                // Redirecting to Login
                return new RedirectToPageResult("/login");
            }
            return Page();
        }
    }
}
