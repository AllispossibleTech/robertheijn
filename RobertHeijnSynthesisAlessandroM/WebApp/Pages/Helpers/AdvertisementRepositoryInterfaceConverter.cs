﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DomainLayer.Interfaces;

namespace WebApp.Pages.Helpers
{
    public class AdvertisementRepositoryInterfaceConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(IAdvertisementRepository).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var obj = serializer.Deserialize<JObject>(reader);
            var typeName = (string)obj["Type"];
            var implementationType = Type.GetType(typeName);
            return obj.ToObject(implementationType, serializer);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var obj = JObject.FromObject(value);
            obj["Type"] = value.GetType().AssemblyQualifiedName;
            obj.WriteTo(writer);
        }
    }
}
