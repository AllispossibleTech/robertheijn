using DataAccessLayer;
using DomainLayer;
using DomainLayer.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Text.Json;
using WebApp.Pages.Helpers;

namespace WebApp.Pages
{
    [Authorize]
    public class shopping_cartModel : PageModel
    {
        [BindProperty]
        public Models.Payment Payment { get; set; }

        public ShoppingCart ShoppingCart { get; private set; }
        public double Total { get; private set; }

        public void OnGet()
        {
            // Retrieving the ShoppingCart from Session
            this.ShoppingCart = SessionHelper.GetCustomObjectFromSession<ShoppingCart>(HttpContext.Session, "ShoppingCart");
            this.ShoppingCart.UseRepository(new AdvertisementRepository());

            // Retrieving the BonusCard of the logged in User
            UserManager userManager = new UserManager(new UserRepository());
            this.Total = this.ShoppingCart.GetTotal(((Customer)userManager.GetUser(Convert.ToInt32(User.FindFirst("id").Value))).BonusCard);
        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                // Retrieving the ShoppingCart from the session
                this.ShoppingCart = SessionHelper.GetCustomObjectFromSession<ShoppingCart>(HttpContext.Session, "ShoppingCart");
                this.ShoppingCart.UseRepository(new AdvertisementRepository());

                // Retrieving the customer that confirmed the order & Adding the order
                UserManager userManager = new UserManager(new UserRepository());
                OrderManager orderManager = new OrderManager(new OrderRepository());
                PaymentMethod paymentMethod = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), this.Payment.Method);
                
                try
                {
                    Customer customer = (Customer)userManager.GetUser(Convert.ToInt32(User.FindFirst("id").Value));
                    orderManager.AddOrder(new Order(this.ShoppingCart, paymentMethod, customer));
                }
                catch (Exception e)
                {
                    string error = e.Message;
                    return RedirectToPage("exception_error");
                }

                // Emptying the ShoppingCart in the session
                this.ShoppingCart = new ShoppingCart(new AdvertisementRepository());
                SessionHelper.SetObjectInSession(HttpContext.Session, "ShoppingCart", new ShoppingCart(new AdvertisementRepository(), new List<CartItem>()));
            }
            return Page();
        }

        public IActionResult OnPostRemove(int? id)
        {
            if (id.HasValue)
            {
                // Retrieving the ShoppingCart from the session
                ShoppingCart shoppingCart = SessionHelper.GetCustomObjectFromSession<ShoppingCart>(HttpContext.Session, "ShoppingCart");
                shoppingCart.UseRepository(new AdvertisementRepository());

                // Removing the CartItem from the Cart
                shoppingCart.Remove(Convert.ToInt32(id));

                // Re-Saving the ShoppingCart in the session
                SessionHelper.SetObjectInSession(HttpContext.Session, "ShoppingCart", shoppingCart);
            }
            return RedirectToPage("/shopping_cart");
        }
    }
}
