using DataAccessLayer;
using DomainLayer;
using DomainLayer.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages
{
    public class order_detailsModel : PageModel
    {
        public Order Order { get; private set; }
        public string Error { get; private set; }
        public Customer Customer { get; private set; }

        public void OnGet(int? id)
        {
            this.Error = "";

            if (id.HasValue)
            {
                UserManager userManager = new UserManager(new UserRepository());
                OrderManager orderManager = new OrderManager(new OrderRepository());

                try
                {
                    Customer customer = (Customer)userManager.GetUser(Convert.ToInt32(User.FindFirst("id").Value));
                    this.Customer = customer;
                    if (userManager.HasOrder(customer, Convert.ToInt32(id)))
                    {
                        this.Order = orderManager.GetOrder(Convert.ToInt32(id));
                    }
                    else
                    {
                        this.Error = "You are not authorized to view this information!";
                    }
                }
                catch(OrderNotFoundException ex)
                {
                    this.Error = ex.Message;
                }
                catch(Exception)
                {
                    Redirect("exception_error");
                }
            }
        }
    }
}
