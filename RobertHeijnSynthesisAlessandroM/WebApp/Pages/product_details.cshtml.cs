using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainLayer;
using DataAccessLayer;
using WebApp.Pages.Helpers;
using System.Text.Json;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace WebApp.Pages
{
    public class product_detailsModel : PageModel
    {
        [BindProperty]
        public Models.CartItem CartItem { get; set; }
        public Product Product { get; set; }
        public void OnGet(int? id)
        {
            if (id.HasValue)
            {
                Stock stock = new Stock(new ProductRepository(), new AdvertisementRepository());
                this.Product = stock.GetProduct(Convert.ToInt32(id));
            }
        }

        public IActionResult OnPost(int? id)
        {
            if (ModelState.IsValid)
            {
                // Retrieving the ShoppingCart from Session
                ShoppingCart shoppingCart = SessionHelper.GetCustomObjectFromSession<ShoppingCart>(HttpContext.Session, "ShoppingCart");
                shoppingCart.UseRepository(new AdvertisementRepository());

                // Retrieving the Product that has to be added to the cart
                Stock stock = new Stock(new ProductRepository(), new AdvertisementRepository());
                this.Product = stock.GetProduct(Convert.ToInt32(id));

                // Adding the product and quantity to the shopping cart
                shoppingCart.Add(this.Product, CartItem.Quantity);

                // Storing ShoppingCart to the session again
                SessionHelper.SetObjectInSession(HttpContext.Session, "ShoppingCart", shoppingCart);
                return new RedirectToPageResult("/shopping_cart");
            }
            return Page();
        }
    }
}
