using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Reflection.PortableExecutable;
using System.Security.Claims;
using DataAccessLayer;
using DomainLayer;
using WebApp.Pages.Helpers;
using System.Text.Json;

namespace WebApp.Pages
{
    public class loginModel : PageModel
    {
        [BindProperty]
        public Models.Credentials Credentials { get; set; }

        public void OnGet()
        {
            if (Request.Cookies.ContainsKey("RememberMeCookieName"))
            {
                this.Credentials = new Models.Credentials();
                this.Credentials.Username = Request.Cookies["RememberMeCookieName"];
                this.Credentials.RememberMe = true;
            }
        }

        public IActionResult OnPost()
        {
            UserManager userManager = new UserManager(new UserRepository());
            
            if (ModelState.IsValid)
            {
                try
                {
                    int userId = userManager.GetUserId(Credentials.Username, Credentials.Password);

                    // Employees cannot login on the Customer view.
                    if (userManager.GetUser(userId) is Employee)
                    {
                        return RedirectToPage("exception_error");
                    }

                    if (userId != -1)
                    {
                        List<Claim> claims = new List<Claim>();
                        claims.Add(new Claim("id", userId.ToString()));
                        claims.Add(new Claim(ClaimTypes.Email, Credentials.Username));

                        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        HttpContext.SignInAsync(new ClaimsPrincipal(claimsIdentity));

                        if (this.Credentials.RememberMe)
                        {
                            CookieOptions cookieOptions = new CookieOptions();
                            cookieOptions.Expires = DateTime.Now.AddDays(1);
                            Response.Cookies.Append("RememberMeCookieName", this.Credentials.Username, cookieOptions);
                        }

                        // Saving a new ShoppingCart to a session
                        SessionHelper.SetObjectInSession(HttpContext.Session, "ShoppingCart", new ShoppingCart(new AdvertisementRepository(), new List<CartItem>()));

                        // Redirecting to index (that will have a different layout now)
                        return new RedirectToPageResult("index");
                    }
                }
                catch (Exception e)
                {
                    throw e;
                    return RedirectToPage("exception_error");
                }
                
            }
            return Page();
        }

    }
}
