using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainLayer;
using DataAccessLayer;
using DomainLayer.Exceptions;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Pages
{
    [Authorize]
    public class view_ordersModel : PageModel
    {
        public List<Order> Orders { get; set; }
        public IActionResult OnGet()
        {
            UserManager userManager = new UserManager(new UserRepository());
            try
            {
                if (userManager.GetUser(Convert.ToInt32(User.FindFirst("id").Value)) is Customer)
                {
                    this.Orders = ((Customer)userManager.GetUser(Convert.ToInt32(User.FindFirst("id").Value))).Orders;
                }
            }
            catch (InvalidIdException e)
            {
                return RedirectToPage("/exception_error");
            }
            return Page();
        }
    }
}
